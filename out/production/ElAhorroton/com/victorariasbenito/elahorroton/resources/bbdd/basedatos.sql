CREATE DATABASE if not exists elahorroton;
--
USE elahorroton;
--
create table if not exists usuarios (
										id int auto_increment primary key not null,
										nick varchar(300) not null unique,
										pass varchar(300) not null,
										nombre varchar(300) not null,
										apellidos varchar(300) not null,
										dni varchar(300) not null unique,
										email varchar(300) not null,
										telefono varchar(300) not null,
										administrador tinyint not null);
--
create table if not exists secciones (
                                        id int auto_increment primary key not null,
                                        nombre varchar(100) not null,
                                        descripcion varchar(100),
                                        habilitada tinyint,
                                        hora_apertura time,
										hora_cierre time,
                                        espacio_total float,
										espacio_ocupado float);
--
create table if not exists facturas(
                                        id int auto_increment primary key not null,
                                        id_usuario int not null,
                                        codigo varchar (100) not null,
                                        fecha_creacion datetime not null,
                                        descuento float not null,
                                        pago float not null,
                                        cambio float not null,
										precio_total float not null);
--

create table if not exists productos(
                                        id int auto_increment primary key not null,
										id_seccion int not null,
                                        nombre varchar(100) not null,
										descripcion varchar(500) not null,
										precio float not null,
                                        fecha_caducidad date not null,
										marca varchar(100) not null,
										imagen longblob);
--

create table if not exists producto_factura(
	id int auto_increment primary key not null,
	id_producto INT UNSIGNED,
	id_factura INT UNSIGNED,
	cantidad INTEGER DEFAULT 0);

/*Triguer que elimina los productos de una factura cuando esta es eliminada*/
delimiter $$
CREATE TRIGGER eliminar_factura AFTER DELETE ON facturas
FOR EACH ROW
BEGIN
	DELETE FROM producto_factura
	WHERE id_factura = OLD.id;
END;
$$ delimiter ;

/*Triguer que elimina los productos de una seccion cuando esta es eliminada*/
delimiter $$
CREATE TRIGGER eliminar_seccion AFTER DELETE ON secciones
FOR EACH ROW
BEGIN
	DELETE FROM productos
	WHERE id_seccion = OLD.id;
END;
$$ delimiter ;

/*Triguer que elimina las facturas generadas por un usuario cuando este es eliminado*/
delimiter $$
CREATE TRIGGER eliminar_usuario AFTER DELETE ON usuarios
FOR EACH ROW
BEGIN
	DELETE FROM facturas
	WHERE id_usuario = OLD.id;
END;
$$ delimiter ;

/*Triguer que se activa al borrar un producto y lo elimina de todas las facturas*/
delimiter $$
CREATE TRIGGER eliminar_producto AFTER DELETE ON productos
FOR EACH ROW
BEGIN
	DELETE FROM producto_factura
	WHERE id_producto = OLD.id;
END;
$$ delimiter ;
	
--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nick`, `pass`, `nombre`, `apellidos`, `dni`, `email`, `telefono`, `administrador`) VALUES
(8, 'XZ/v35+hUK5N89eMAT6W+g==', '1ARVn2Auq2/WAqx2gNrL+q3RNjAzXpUfCXrzkA6d4Xa22yhRLy4AC50E+6UTPoscbo31nbOoq51gvkuXzJ6B2w==', 'XZ/v35+hUK5N89eMAT6W+g==', 'XZ/v35+hUK5N89eMAT6W+g==', 'XZ/v35+hUK5N89eMAT6W+g==', 'XZ/v35+hUK5N89eMAT6W+g==', 'IAVfGMjmIdW7bkJTyR8EZw==', 1);


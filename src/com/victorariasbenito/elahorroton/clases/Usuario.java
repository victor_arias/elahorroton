package com.victorariasbenito.elahorroton.clases;

import com.victorariasbenito.elahorroton.libreriasFunciones.Cifrado;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "usuarios", schema = "elahorroton", catalog = "")
public class Usuario {
    private int id;
    private String nick;
    private String pass;
    private String nombre;
    private String apellidos;
    private String dni;
    private String email;
    private String telefono;
    private boolean administrador;
    private List<Factura> facturas;

    /*Variables extra*/
    private boolean cifrado;
    private boolean hasheado;

    /**
     * Constructor de la clase que inicializa el indicador de incriptación a true
     * se le modificará el valor al crearlo mediante el seter.
     */
    public Usuario(){
        this.cifrado = false;
        this.hasheado = false;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nick")
    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    @Basic
    @Column(name = "pass")
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Basic
    @Column(name = "administrador")
    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return id == usuario.id &&
                administrador == usuario.administrador &&
                Objects.equals(nick, usuario.nick) &&
                Objects.equals(pass, usuario.pass) &&
                Objects.equals(nombre, usuario.nombre) &&
                Objects.equals(apellidos, usuario.apellidos) &&
                Objects.equals(dni, usuario.dni) &&
                Objects.equals(email, usuario.email) &&
                Objects.equals(telefono, usuario.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nick, pass, nombre, apellidos, dni, email, telefono, administrador);
    }

    @OneToMany(mappedBy = "usuario")
    public List<Factura> getFacturas() {
        return facturas;
    }

    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }


    /*Funciones extra*/

    public void setCifrado(boolean cifrado) {
        this.cifrado = cifrado;
    }

    /**
     * Función que cifrar los datos de este usuario y sobreescribe sus valores en el caso  encritado != true
     */
    public void cifrar(){
        if(!cifrado){
            try {
                this.nick = Cifrado.cifrar(this.nick);

                this.nombre = Cifrado.cifrar(this.nombre);
                this.apellidos = Cifrado.cifrar(this.apellidos);
                this.dni = Cifrado.cifrar(this.dni);
                this.email = Cifrado.cifrar(this.email);
                this.telefono = Cifrado.cifrar(this.telefono);

            } catch (Exception e) {
                e.printStackTrace();
            }

            this.cifrado = true;
        }
    }

    /**
     * Función que descifrar los datos de este usuario y sobreescribe sus valores en el caso  encritado == true
     */
    public void descifrar(){
        if(cifrado){
            try {
                this.nick = Cifrado.descifrar(this.nick);

                this.nombre = Cifrado.descifrar(this.nombre);
                this.apellidos = Cifrado.descifrar(this.apellidos);
                this.dni = Cifrado.descifrar(this.dni);
                this.email = Cifrado.descifrar(this.email);
                this.telefono = Cifrado.descifrar(this.telefono);

            } catch (Exception e) {
                e.printStackTrace();
            }

            this.cifrado = false;
        }
    }

    public void hashear(){
        if(!this.hasheado){
            try {
                this.pass = Cifrado.hashear(this.pass);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.hasheado = true;
    }

    /**
     * Funcion que comprueba el hasheo de una contraseña con la contraseña
     * @param passSinCifrar Contraseña en formato String
     * @return true si las contraseñas son iguales false en caso contrario
     */
    public boolean comprobarPass(String passSinCifrar){
        try {
            return Cifrado.comprobarHasheo(this.pass, passSinCifrar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String toString() {
        descifrar();
        String salida = "Nick: " + nick +
                ", Nombre: " + nombre +
                ", Apellidos: " + apellidos +
                ", Email: " + email +
                ", Telefono: " + telefono +
                ", DNI: " + dni +
                ", Administrador: " + administrador;
        cifrar();
        return salida;
    }
}

package com.victorariasbenito.elahorroton.clases;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "producto_factura", schema = "elahorroton", catalog = "")
public class ProductoFactura {
    private static int ID_COUNT = 0; //Id utilizado en la generación de facturas
    private int id;
    private int cantidad;
    private Factura factura;
    private Producto producto;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductoFactura that = (ProductoFactura) o;
        return id == that.id &&
                cantidad == that.cantidad;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cantidad);
    }

    @ManyToOne
    @JoinColumn(name = "id_factura", referencedColumnName = "id", nullable = false)
    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    @ManyToOne
    @JoinColumn(name = "id_producto", referencedColumnName = "id", nullable = false)
    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @Override
    public String toString() {
        return "Cantidad: " + cantidad +
                ", Producto: " + producto.getNombre() +
                ", Precio del producto: " + producto.getPrecio();
    }

    public void generarId(){
        this.id = ID_COUNT;
        ID_COUNT++;
    }
}

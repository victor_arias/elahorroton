package com.victorariasbenito.elahorroton.clases;

import javax.persistence.*;
import java.sql.Time;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "secciones", schema = "elahorroton", catalog = "")
public class Seccion {
    private int id;
    private String nombre;
    private String descripcion;
    private boolean habilitada;
    private Time horaApertura;
    private Time horaCierre;
    private double espacioTotal;
    private double espacioOcupado;
    private List<Producto> productos;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "habilitada")
    public boolean isHabilitada() {
        return habilitada;
    }

    public void setHabilitada(boolean habilitada) {
        this.habilitada = habilitada;
    }

    @Basic
    @Column(name = "hora_apertura")
    public Time getHoraApertura() {
        return horaApertura;
    }

    public void setHoraApertura(Time horaApertura) {
        this.horaApertura = horaApertura;
    }

    @Basic
    @Column(name = "hora_cierre")
    public Time getHoraCierre() {
        return horaCierre;
    }

    public void setHoraCierre(Time horaCierre) {
        this.horaCierre = horaCierre;
    }

    @Basic
    @Column(name = "espacio_total")
    public double getEspacioTotal() {
        return espacioTotal;
    }

    public void setEspacioTotal(double espacioTotal) {
        this.espacioTotal = espacioTotal;
    }

    @Basic
    @Column(name = "espacio_ocupado")
    public double getEspacioOcupado() {
        return espacioOcupado;
    }

    public void setEspacioOcupado(double espacioOcupado) {
        this.espacioOcupado = espacioOcupado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seccion seccion = (Seccion) o;
        return id == seccion.id &&
                habilitada == seccion.habilitada &&
                Double.compare(seccion.espacioTotal, espacioTotal) == 0 &&
                Double.compare(seccion.espacioOcupado, espacioOcupado) == 0 &&
                Objects.equals(nombre, seccion.nombre) &&
                Objects.equals(descripcion, seccion.descripcion) &&
                Objects.equals(horaApertura, seccion.horaApertura) &&
                Objects.equals(horaCierre, seccion.horaCierre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, descripcion, habilitada, horaApertura, horaCierre, espacioTotal, espacioOcupado);
    }

    @OneToMany(mappedBy = "seccion")
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    @Override
    public String toString() {
        return "Nombre: " + nombre +
                ", Habilitada: " + habilitada +
                ", EspacioTotal: " + espacioTotal +
                ", EspacioOcupado: " + espacioOcupado;
    }
    
}

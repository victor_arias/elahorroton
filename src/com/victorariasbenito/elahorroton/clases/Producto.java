package com.victorariasbenito.elahorroton.clases;

import javax.persistence.*;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "productos", schema = "elahorroton", catalog = "")
public class Producto {
    private int id;
    private String nombre;
    private String descripcion;
    private double precio;
    private Date fechaCaducidad;
    private String marca;
    private byte[] imagen;
    private Seccion seccion;
    private List<ProductoFactura> productoFactura;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "fecha_caducidad")
    public Date getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(Date fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "imagen")
    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return id == producto.id &&
                Double.compare(producto.precio, precio) == 0 &&
                Objects.equals(nombre, producto.nombre) &&
                Objects.equals(descripcion, producto.descripcion) &&
                Objects.equals(fechaCaducidad, producto.fechaCaducidad) &&
                Objects.equals(marca, producto.marca) &&
                Arrays.equals(imagen, producto.imagen);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, nombre, descripcion, precio, fechaCaducidad, marca);
        result = 31 * result + Arrays.hashCode(imagen);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "id_seccion", referencedColumnName = "id", nullable = false)
    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

    @OneToMany(mappedBy = "producto")
    public List<ProductoFactura> getProductoFactura() {
        return productoFactura;
    }

    public void setProductoFactura(List<ProductoFactura> productoFactura) {
        this.productoFactura = productoFactura;
    }

    @Override
    public String toString() {
        return "Nombre: " + nombre +
                ", Precio: " + precio +
                ", Marca: " + marca +
                ", Seccion: " + seccion.getNombre();
    }
}

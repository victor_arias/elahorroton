package com.victorariasbenito.elahorroton.clases;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "facturas", schema = "elahorroton", catalog = "")
public class Factura {
    private int id;
    private String codigo;
    private LocalDateTime fechaCreacion;
    private double descuento;
    private double pago;
    private double cambio;
    private double precioTotal;
    private Usuario usuario;
    private List<ProductoFactura> productoFactura;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "fecha_creacion")
    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Basic
    @Column(name = "descuento")
    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    @Basic
    @Column(name = "pago")
    public double getPago() {
        return pago;
    }

    public void setPago(double pago) {
        this.pago = pago;
    }

    @Basic
    @Column(name = "cambio")
    public double getCambio() {
        return cambio;
    }

    public void setCambio(double cambio) {
        this.cambio = cambio;
    }

    @Basic
    @Column(name = "precio_total")
    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Factura factura = (Factura) o;
        return id == factura.id &&
                Double.compare(factura.descuento, descuento) == 0 &&
                Double.compare(factura.pago, pago) == 0 &&
                Double.compare(factura.cambio, cambio) == 0 &&
                Double.compare(factura.precioTotal, precioTotal) == 0 &&
                Objects.equals(codigo, factura.codigo) &&
                Objects.equals(fechaCreacion, factura.fechaCreacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codigo, fechaCreacion, descuento, pago, cambio, precioTotal);
    }

    @ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", nullable = false)
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @OneToMany(mappedBy = "factura")
    public List<ProductoFactura> getProductoFactura() {
        return productoFactura;
    }

    public void setProductoFactura(List<ProductoFactura> productoFactura) {
        this.productoFactura = productoFactura;
    }

    @Override
    public String toString() {
        return "Codigo: " + codigo +
                ", Pago: " + pago +
                ", Cambio: " + cambio +
                ", PrecioTotal: " + precioTotal;
    }
}

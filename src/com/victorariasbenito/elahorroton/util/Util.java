package com.victorariasbenito.elahorroton.util;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Esta es una clase en la que tengo métodos estáticos para crear una ventana con un mensaje.
 * Cada método se refiere a un tipo distinto de mensaje.
 */
public class Util {
    /**
     * Este método me muestra un mensaje de error con el texto recibido
     * @param message Texto del mensaje de error
     */
    public static void showErrorAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
    /**
     * Este método me muestra un mensaje de aviso con el texto recibido
     * @param message Texto del mensaje de aviso
     */
    public static void showWarningAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Aviso", JOptionPane.WARNING_MESSAGE);
    }
    /**
     * Este método me muestra un mensaje de información con el texto recibido
     * @param message Texto del mensaje de información
     */
    public static void showInfoAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Información", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Este método me muestra un mensaje de confirmación y devuelve lo que yo le diga
     * @param message
     * @return Devuelve un int con el resultado
     */
    public static boolean confirmationAlert(String message){
        int input = JOptionPane.showConfirmDialog(null, message , "Confirmar",JOptionPane.YES_NO_CANCEL_OPTION);
        if(input == 0){
            return true;
        }
        return false;
    }

    /**
     * Este método me muestra un jFileChooser para seleccionar la imagen de lproducto
     * @return Dato tipo Image con la imagen escojida en el fileChooser
     * @throws IOException
     */
    public static Image showJFileChooser() throws IOException {
        JFileChooser selector = new JFileChooser();
        int opcion = selector.showOpenDialog(null);
        if(opcion == JFileChooser.APPROVE_OPTION){ //Ha pulsado Aceptar
            File fichero = selector.getSelectedFile();
            Image image = ImageIO.read(fichero);
            return image;
        }
        return null;
    }
}
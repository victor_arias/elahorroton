package com.victorariasbenito.elahorroton.gui;

import com.victorariasbenito.elahorroton.clases.*;
import com.victorariasbenito.elahorroton.clases.*;
import com.victorariasbenito.elahorroton.libreriasFunciones.Cifrado;
import com.victorariasbenito.elahorroton.libreriasFunciones.TrabajarImagenes;
import com.victorariasbenito.elahorroton.util.Util;

import javax.imageio.ImageIO;
import javax.persistence.PersistenceException;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 *
 */
public class Controlador implements ActionListener, ListSelectionListener, ItemListener, WindowListener, MouseListener {
    private Vista vista;
    private Modelo modelo;
    private Usuario usuarioLogueado;
    private ArrayList<ProductoFactura> productosFacturaDeLaFacturaActual;
    private Factura facturaActual;

    public Controlador(Vista vista, Modelo modelo) {

        this.vista = vista;
        this.modelo = modelo;
        /*Bloqueo acceso a las zonas sensibles por motivos de seguridad lo hago lo primero*/
        ocultarTodosLosObjetos();

        this.usuarioLogueado = null;
        this.productosFacturaDeLaFacturaActual = new ArrayList<ProductoFactura>();
        this.facturaActual = new Factura();

        /*Pego imagen base*/
        pegarImagenBase();

        this.modelo.conectar();

        //Listo Objetos
        listarProductos(modelo.getProductos());
        listarSecciones(modelo.getSecciones());
        listarFacturas(modelo.getFacturas());
        listarUsuarios(modelo.getUsuarios());
        listarMarcasProducto(modelo.getMarcasDeProductos());

        //Checkboxes -->
        refrescarMarcasProductos();
        refrescarSeccinesProducto();
        refrescarSeccines();
        refrescarProductos();

        //Añadimos action listeners que permiten, una vez cargado el programa, la utilización del mismo
        addActionListeners(this);
        addListSelectionListener(this);
        addMouseListener(this);
    }

    /****************************************************************************************************************/

    /**
     * Anyadimos los listeners a los elementos graficos
     * @param listener
     */
    private void addActionListeners(ActionListener listener){
        /*Menu*/
        vista.conexionItem.addActionListener(listener);
        vista.opcionesItem.addActionListener(listener);
        vista.loguearseItem.addActionListener(listener);
        vista.registrarseItem.addActionListener(listener);
        vista.informacionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);

        vista.perfilItem.addActionListener(listener);
        vista.cerrarSesionItem.addActionListener(listener);

        /*OpcionesBbdd*/
        vista.opcionesBbdd.btnGuardarOpcionesBbdd.addActionListener(listener);
        vista.opcionesBbdd.establecerPorDefectoButton.addActionListener(listener);

        /*Loguearse*/
        vista.loguearse.loginButtonLoguearse.addActionListener(listener);

        /*Registrarse*/
        vista.registrarse.registrarseButtonRegistrarse.addActionListener(listener);

        /*Perfil*/
        vista.perfil.eliminarPerfilButton.addActionListener(listener);
        vista.perfil.modificarPerfilButton.addActionListener(listener);
        vista.perfil.cambiarContrasenaButton.addActionListener(listener);

        /*Productos*/
        vista.altaProductoButton.addActionListener(listener);
        vista.borrarProductoButton.addActionListener(listener);
        vista.modificarProductoButton.addActionListener(listener);
        vista.listarProductosButton.addActionListener(listener);

        /*Secciones*/
        vista.altaSeccionesButton.addActionListener(listener);
        vista.borrarSeccionesButton.addActionListener(listener);
        vista.modificarSeccionesButton.addActionListener(listener);
        vista.listarSeccionesButton.addActionListener(listener);

        /*BuscarProductos*/
        vista.buscarPorSeccionButton.addActionListener(listener);
        vista.buscarPorMarcaButton.addActionListener(listener);
        vista.pdfProductosPorSeccionButton.addActionListener(listener);

        /*Facturas*/
        vista.altaFacturaButton.addActionListener(listener);
        vista.borrarFacturaButton.addActionListener(listener);
        vista.modificarFacturaButton.addActionListener(listener);
        vista.listarFacturasButton.addActionListener(listener);
        vista.anadirALaFacturaButton.addActionListener(listener);
        vista.modificarProductoFacturaButton.addActionListener(listener);
        vista.eliminarProductoFacturaButton.addActionListener(listener);

        /*Usuarios*/
        vista.listarUsuariosButton.addActionListener(listener);
        vista.hacerAdministradorUsuarioButton.addActionListener(listener);
        vista.borrarUsuarioButton.addActionListener(listener);
    }

    /**
     * Añade los listeners a las listas de Objetos
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener){
        vista.listUsuarios.addListSelectionListener(listener);
        vista.listSecciones.addListSelectionListener(listener);
        vista.listProductos.addListSelectionListener(listener);
        vista.listFacturas.addListSelectionListener(listener);
        vista.listProductoFacturas.addListSelectionListener(listener);
        vista.listBuscarProductos.addListSelectionListener(listener);
    }

    /**
     * Añade los listeners de raton a objetos
     * @param mouseListener
     */
    private void addMouseListener(MouseListener mouseListener){
        vista.imagenProductos.addMouseListener(mouseListener);
    }
    /****************************************************************************************************************/

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Salir":
                //modelo.desconectar();
                System.exit(0);
                break;

            case "Informacion":
                String mensajeInformacion = "En la empresa 'El Ahorroton' nos encargamos de que pueda gestionar\n" +
                        " un supermercado de forma sencilla, acceda a las secciones disponibles en la pestaña principal\n" +
                        "o directamente habra el menú de opciones donde encontrará un loguin, registro, y muchas más\n" +
                        "utilidades si necesita configurar los datos de acceso a su base de datos habra el apartado\n" +
                        " OpcionesConexion si desea registrarse para acceder a más privilegios clique en la sección\n" +
                        " Registrarse recuerde que un usuario administrador podría eliminarle en caso de no desear\n" +
                        " su presencia";
                Util.showInfoAlert(mensajeInformacion);
                break;

            case "Conectar":
                //vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;

            /*Opciones de conexion*/
            case "OpcionesConexion":
                    vista.opcionesBbdd.txtUsuarioOpcionesBbdd.setText(modelo.userBBDD);
                    vista.opcionesBbdd.passOpcionesBbdd.setText(modelo.passBBDD);
                    vista.opcionesBbdd.setVisible(true);
                break;

            case "Establecer por defecto":
                modelo.setValuesDefault();
                vista.opcionesBbdd.txtUsuarioOpcionesBbdd.setText(modelo.userBBDD);
                vista.opcionesBbdd.passOpcionesBbdd.setText(modelo.passBBDD);
                break;

            case "GuardarOpcionesBbdd":
                try{
                    modelo.userBBDD = vista.opcionesBbdd.txtUsuarioOpcionesBbdd.getText();
                    modelo.passBBDD = String.valueOf(vista.opcionesBbdd.passOpcionesBbdd.getPassword());
                    modelo.setPropValues(modelo.userBBDD, modelo.passBBDD);

                    vista.opcionesBbdd.dispose();
                    modelo.conectar();
                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            /*Loguearse*/
            case "Loguearse":
                vista.loguearse.setVisible(true);
                break;

            case "Loguearse Button":
                try{
                    String usuarioIntroducido = vista.loguearse.txtUsuarioLoguearse.getText();
                    String passIntroducida = vista.loguearse.passwordField1Loguearse.getText();
                    Usuario unUsuario = null;
                    try {
                        unUsuario = modelo.getUsuario(Cifrado.cifrar(usuarioIntroducido));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    if(unUsuario != null){
                        if(unUsuario.comprobarPass(passIntroducida)){
                            acctualizarUsuarioRegistrado(unUsuario);
                            vista.loguearse.setVisible(false);
                            borrarCamposLoguin();
                            Util.showInfoAlert("Logueado con exito");
                        }else{
                            Util.showWarningAlert("Contraseña incorrecta");
                        }
                    }else{
                        Util.showWarningAlert("El usuario no existe");
                    }
                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            /*Cerrar sesion*/
            case "Cerrar Sesion":
                vista.perfil.setVisible(false); //Cerramos ventanas emergentes relativas al usuario
                cerrarSesionUsuario();
                Util.showInfoAlert("Se ha cerrado la sesion");
                break;
            /*Registrarse*/
            case "Registrarse":
                vista.registrarse.setVisible(true);
                break;

            case "Registrarse Button":
                try{
                    if(modelo.existeUsuario(vista.registrarse.txtUsuarioRegistrarse.getText())){ //Existe el usuario
                        Util.showInfoAlert("El usuario ya existe");
                    }else if (modelo.existeDni(vista.registrarse.txtDniRegistrarse.getText())){
                        Util.showInfoAlert("El dni ya existe");
                    }else{
                        Usuario nuevoUsuario = new  Usuario();
                        nuevoUsuario.setCifrado(false); //Indicamos que el contenido no esta cifrado
                        nuevoUsuario.setNick(vista.registrarse.txtUsuarioRegistrarse.getText());
                        //Si coinciden las contraseñas guardamso la primera y la asiganamos
                        String pass1Introducida = vista.registrarse.passwordField1Registrarse.getText();
                        String pass2Introducida = vista.registrarse.passwordField2Registrarse.getText();
                        if(pass1Introducida.equals(pass2Introducida)){
                            try {
                                nuevoUsuario.setPass(pass1Introducida);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }else{
                            Util.showWarningAlert("Las contraseñas no coinciden");
                        }
                        nuevoUsuario.setNombre(vista.registrarse.txtNombreRegistrarse.getText());
                        nuevoUsuario.setApellidos(vista.registrarse.txtApellidosRegistrarse.getText());
                        nuevoUsuario.setDni(vista.registrarse.txtDniRegistrarse.getText());
                        nuevoUsuario.setEmail(vista.registrarse.txtEmailRegistrarse.getText());
                        nuevoUsuario.setTelefono(vista.registrarse.txtTelefonoRegistrarse.getText());
                        nuevoUsuario.setAdministrador(false);

                        nuevoUsuario.cifrar();
                        nuevoUsuario.hashear();
                        modelo.altaUsuario(nuevoUsuario);
                        Util.showInfoAlert("Usuario registrado correctamente");
                        borrarCamposRegistro();
                    }
                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            /*Perfil*/
            case "EditarPerfil":
                try{
                    Usuario usuarioSeleccion = this.usuarioLogueado;
                    usuarioSeleccion.setCifrado(true); //Indicamos que este contenido esta cifrado
                    usuarioSeleccion.descifrar();//Desciframos los datos del usuario

                    vista.perfil.txtUsuarioPerfil.setText(usuarioSeleccion.getNick());
                    vista.perfil.txtNombrePerfil.setText(usuarioSeleccion.getNombre());
                    vista.perfil.txtApellidosPerfil.setText(usuarioSeleccion.getApellidos());
                    vista.perfil.txtDniPerfil.setText(usuarioSeleccion.getDni());
                    vista.perfil.txtEmailPerfil.setText(usuarioSeleccion.getEmail());
                    vista.perfil.txtTelefonoPerfil.setText(usuarioSeleccion.getTelefono());
                    vista.perfil.checkBoxAdministradorPerfil.setSelected(usuarioSeleccion.isAdministrador());

                    usuarioSeleccion.cifrar(); //Ciframos los datos del usuario

                    vista.perfil.setVisible(true); //Mostramos la ventana con todos los datos
                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            case "Modificar Perfil":
                try {
                    String nuevoNick = vista.perfil.txtUsuarioPerfil.getText();
                    String nuevoDni = vista.perfil.txtDniPerfil.getText();
                    Boolean existeEnBBDD = modelo.existeUsuario(nuevoNick);
                    Boolean existeDniEnBBDD = modelo.existeDni(nuevoDni);
                    Usuario usuarioModificar = this.usuarioLogueado;
                    usuarioModificar.descifrar();
                    if(existeEnBBDD && (!nuevoNick.equals(usuarioModificar.getNick()))) { //Existe el usuario
                        Util.showInfoAlert("El usuario ya existe");
                        usuarioModificar.cifrar();
                    }else if(existeDniEnBBDD && (!nuevoDni.equals(usuarioModificar.getDni()))){
                        Util.showInfoAlert("El dni ya existe");
                        usuarioModificar.cifrar();
                    }else {
                        usuarioModificar.setNick(vista.perfil.txtUsuarioPerfil.getText());
                        usuarioModificar.setNombre(vista.perfil.txtNombrePerfil.getText());
                        usuarioModificar.setApellidos(vista.perfil.txtApellidosPerfil.getText());
                        usuarioModificar.setDni(vista.perfil.txtDniPerfil.getText());
                        usuarioModificar.setEmail(vista.perfil.txtEmailPerfil.getText());
                        usuarioModificar.setTelefono(vista.perfil.txtTelefonoPerfil.getText());
                        usuarioModificar.setAdministrador(this.usuarioLogueado.isAdministrador());

                        usuarioModificar.cifrar();
                        modelo.modificarUsuario(usuarioModificar);
                        Util.showInfoAlert("Usuario modificado correctamente");
                    }
                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            case "Eliminar Perfil":
                if(Util.confirmationAlert("¿Desea eliminar su usuario permanentemente?")){
                    vista.perfil.setVisible(false); //Cerramos ventanas emergentes relativas al usuario
                    modelo.borrarUsuario(this.usuarioLogueado); //Borramos el usuario
                    cerrarSesionUsuario(); //Cerramos sesion
                }
                break;

            case "Cambiar Contrasena Perfil":
               try{
                   this.usuarioLogueado.descifrar();
                   Boolean existeEnBBDD2 = modelo.existeUsuario(this.usuarioLogueado.getNick());
                   this.usuarioLogueado.cifrar();
                   if(existeEnBBDD2){ //Existe el usuario
                       //Comprobamos que la contraseña es correcta
                       if(this.usuarioLogueado.comprobarPass(vista.perfil.passwordFieldViejaContrasenaPerfil.getText())){
                           String nuevaPass1 = vista.perfil.passwordFieldNuevaContrasenaPerfil.getText();
                           String nuevaPass2 = vista.perfil.passwordFieldRepetirContrasenaPerfil.getText();
                           if(nuevaPass1.equals(nuevaPass2)){ //Comprobamos que coincidan ambas contraseñas
                               try {
                                   this.usuarioLogueado.setPass(nuevaPass1); //Asignamos pass al usuario
                                   this.usuarioLogueado.hashear(); //Hasheamos la pass
                                   modelo.modificarUsuario(this.usuarioLogueado); //Actualizamos el usuario
                                   Util.showInfoAlert("Se ha modificado su contraseña correctamente");
                                   borrarCamposPerfil();
                               } catch (Exception ex) {
                                   Util.showErrorAlert("Fallo al enviar o hashear");
                                   ex.printStackTrace();
                               }
                           }else{
                               Util.showWarningAlert("Las nuevas contraseñas no coinciden");
                           }
                       }else{
                           Util.showWarningAlert("La contraseña no es correcta");
                       }
                   }else {
                       Util.showWarningAlert("El usuario no existe");
                   }
                   this.usuarioLogueado.cifrar();
                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            /*Producto*/
            case "Alta Producto":
                try{
                    Producto nuevoProducto = new  Producto();
                    nuevoProducto.setNombre(vista.txtNombreProductos.getText());
                    nuevoProducto.setPrecio(Double.parseDouble(vista.txtPrecioProductos.getText()));
                    nuevoProducto.setFechaCaducidad(Date.valueOf(vista.datePickerProductos.getDate()));
                    nuevoProducto.setMarca(vista.txtMarcaProductos.getText());
                    nuevoProducto.setDescripcion(vista.txtDescripcionProductos.getText());
                    nuevoProducto.setSeccion((Seccion) vista.comboBoxSeccionProductos.getSelectedItem());

                    if(nuevoProducto.getSeccion() == null){
                        Util.showWarningAlert("Es necesario seleccionar una sección");
                    }

                    byte[] bt = TrabajarImagenes.iconToBytes(vista.imagenProductos.getIcon());
                    nuevoProducto.setImagen(bt);

                    modelo.altaProducto(nuevoProducto);
                    //PersistenceException
                    borrarCamposProductos();

                    refrescarMarcasProductos();

                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                } catch (IOException ex) {
                    Util.showErrorAlert("Error al cargar la imagen");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            case "Listar Productos":
                listarProductos(modelo.getProductos());
                break;

            case "Modificar Producto":
                try{
                    Producto productoSeleccion = (Producto)vista.listProductos.getSelectedValue();
                    productoSeleccion.setNombre(vista.txtNombreProductos.getText());
                    productoSeleccion.setPrecio(Double.parseDouble(vista.txtPrecioProductos.getText()));
                    productoSeleccion.setFechaCaducidad(Date.valueOf(vista.datePickerProductos.getDate()));
                    productoSeleccion.setMarca(vista.txtMarcaProductos.getText());
                    productoSeleccion.setDescripcion(vista.txtDescripcionProductos.getText());
                    productoSeleccion.setSeccion((Seccion) vista.comboBoxSeccionProductos.getSelectedItem());

                    if(productoSeleccion.getSeccion() == null){
                        Util.showWarningAlert("Es necesario seleccionar una sección");
                    }else {
                        byte[] bt = TrabajarImagenes.iconToBytes(vista.imagenProductos.getIcon());
                        productoSeleccion.setImagen(bt);
                    }

                    modelo.modificarProducto(productoSeleccion);
                    borrarCamposProductos();
                    refrescarMarcasProductos();

                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                } catch (IOException ex) {
                    Util.showErrorAlert("Error al cargar la imagen");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            case "Borrar Producto":
                try {
                    Producto productoBorrado = (Producto) vista.listProductos.getSelectedValue();
                    modelo.borrarProducto(productoBorrado);
                    listarProductos(modelo.getProductos());
                    borrarCamposProductos();
                    refrescarMarcasProductos();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Seleccione un producto a eliminar");
                    ex.printStackTrace();
                }
                break;

            /*Seccion*/
            case "Alta Seccion":
                try{
                    Seccion nuevaSeccion = new  Seccion();
                    nuevaSeccion.setNombre(vista.txtNombreSecciones.getText());
                    nuevaSeccion.setDescripcion(vista.txtDescripcionSecciones.getText());

                    Boolean habilitada = vista.checkBoxHabilitadaSecciones.isSelected();
                    if(habilitada == null){ habilitada = false;}
                    nuevaSeccion.setHabilitada(habilitada);

                    nuevaSeccion.setHoraApertura(Time.valueOf(vista.horaAperturaSecciones.getTime()));
                    nuevaSeccion.setHoraCierre(Time.valueOf(vista.horaCierreSecciones.getTime()));
                    nuevaSeccion.setEspacioTotal(Double.parseDouble(String.valueOf(vista.spinnerEspacioTotalSecciones.getValue())));
                    nuevaSeccion.setEspacioOcupado(Double.parseDouble(String.valueOf(vista.spinnerEspacioOcupadoSecciones.getValue())));

                    modelo.altaSeccion(nuevaSeccion);
                    borrarCamposSecciones();
                    refrescarSeccinesProducto();

                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            case "Listar Seccion":
                listarSecciones(modelo.getSecciones());
                break;

            case "Modificar Seccion":
                try{
                    Seccion seccionSeleccion = (Seccion)vista.listSecciones.getSelectedValue();
                    seccionSeleccion.setNombre(vista.txtNombreSecciones.getText());
                    seccionSeleccion.setDescripcion(vista.txtDescripcionSecciones.getText());

                    Boolean habilitada2 = vista.checkBoxHabilitadaSecciones.isSelected();
                    if(habilitada2 == null){ habilitada2 = false;}
                    seccionSeleccion.setHabilitada(habilitada2);

                    seccionSeleccion.setHoraApertura(Time.valueOf(vista.horaAperturaSecciones.getTime()));
                    seccionSeleccion.setHoraCierre(Time.valueOf(vista.horaCierreSecciones.getTime()));
                    seccionSeleccion.setEspacioTotal(Double.parseDouble(String.valueOf(vista.spinnerEspacioTotalSecciones.getValue())));
                    seccionSeleccion.setEspacioOcupado(Double.parseDouble(String.valueOf(vista.spinnerEspacioOcupadoSecciones.getValue())));

                    modelo.modificarSeccion(seccionSeleccion);
                    borrarCamposSecciones();
                    refrescarSeccinesProducto();

                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            case "Borrar Seccion":
                try {
                    if(Util.confirmationAlert("Todos los productos de esta sección se borrarán\n ¿Esta seguro de que desea eliminar la sección?")){
                        Seccion seccionBorrada = (Seccion) vista.listSecciones.getSelectedValue();
                        modelo.borrarSeccion(seccionBorrada);
                        listarSecciones(modelo.getSecciones());
                        borrarCamposSecciones();
                        refrescarSeccinesProducto();
                    }
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Seleccione una seccion a eliminar");
                    ex.printStackTrace();
                }
                break;

            /*BuscarProductos*/
            case "Buscar por seccion":
                Seccion seccion = (Seccion) vista.comboBoxSeccionBuscarProductos.getSelectedItem();
                listarBuscarProductos(modelo.getProductosPorSeccion(seccion));
                break;

            case "Buscar por marca":
                try {
                    String marca = vista.comboBoxMarcaBuscarProductos.getSelectedItem().toString();
                    listarBuscarProductos(modelo.getProductosPorMarca(marca));
                }catch (NullPointerException ex){
                    ex.printStackTrace();
                }
                break;

            case "PDF productos por seccion":
                String direccionFicheroOriginal = "src/com/victorariasbenito/elahorroton/resources/jaspersoft/elAhorroton.jasper";
                String direccionNuevoFichero = "src/com/victorariasbenito/elahorroton/resources/ficheros/productosPorSeccion.pdf";
                modelo.generarInformeJasperSoft(direccionFicheroOriginal, direccionNuevoFichero);
                break;

            /*Factura*/
            case "Anadir A La Factura":
                try{
                    ProductoFactura nuevoProductoFactura = new ProductoFactura();
                    nuevoProductoFactura.generarId(); //Generamos un id que eliminaremos antes de subirlo a BBDD
                    nuevoProductoFactura.setCantidad(Integer.parseInt(String.valueOf(vista.spinnerCantidadFacturas.getValue())));
                    nuevoProductoFactura.setProducto((Producto) vista.comboBoxProductoFacturas.getSelectedItem());
                    nuevoProductoFactura.setFactura(this.facturaActual);
                    this.productosFacturaDeLaFacturaActual.add(nuevoProductoFactura);

                    listarProductosFactura(this.productosFacturaDeLaFacturaActual);
                    calcularPrecioFactura(this.productosFacturaDeLaFacturaActual); //Mostramos precio total

                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            case "Modificar Producto Factura":
                try{
                    //Obtengo la posicion en mi arraylist del producto seleccionado en la lista
                    int posicion = productosFacturaDeLaFacturaActual.indexOf((vista.listProductoFacturas.getSelectedValue()));

                    int cantidad = Integer.parseInt(String.valueOf(vista.spinnerCantidadFacturas.getValue()));
                    Producto productoAux = (Producto) vista.comboBoxProductoFacturas.getSelectedItem();

                    ProductoFactura productoFacturaModificado = ((ProductoFactura) vista.listProductoFacturas.getSelectedValue());
                    productoFacturaModificado.setCantidad(cantidad);
                    productoFacturaModificado.setProducto(productoAux);

                    //Modifico el producto seleccionado en mi lista con los nuevos datos
                    productosFacturaDeLaFacturaActual.set(posicion, productoFacturaModificado);

                    listarProductosFactura(this.productosFacturaDeLaFacturaActual);
                    calcularPrecioFactura(this.productosFacturaDeLaFacturaActual); //Mostramos precio total
                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            case "Eliminar Producto Factura":
                try {
                    int posicion2 = productosFacturaDeLaFacturaActual.indexOf((vista.listProductoFacturas.getSelectedValue()));

                    //Elimino el producto seleccionado en mi lista con los nuevos datos
                    productosFacturaDeLaFacturaActual.remove(posicion2);

                    listarProductosFactura(this.productosFacturaDeLaFacturaActual);
                    calcularPrecioFactura(this.productosFacturaDeLaFacturaActual); //Mostramos precio total
                }catch (ArrayIndexOutOfBoundsException ex){
                    Util.showWarningAlert("Seleccione un producto a eliminar");
                }
                break;

            case "Alta Factura":
                try{
                    Factura nuevaFactura = new  Factura();
                    nuevaFactura.setCodigo(vista.txtCodigoFacturas.getText());
                    nuevaFactura.setDescuento(Double.parseDouble(String.valueOf(vista.txtDescuentoFacturas.getText())));
                    nuevaFactura.setPago(Double.parseDouble(String.valueOf(vista.txtPagoFacturas.getText())));
                    nuevaFactura.setCambio(Double.parseDouble(String.valueOf(vista.txtCambioFacturas.getText())));
                    nuevaFactura.setPrecioTotal(Double.parseDouble(String.valueOf(vista.txtPrecioFacturas.getText())));

                    nuevaFactura.setUsuario(this.usuarioLogueado); //Asignamos usuario a la factura
                    nuevaFactura.setFechaCreacion(LocalDateTime.now()); //Establecemos la fecha de creacion de la factura
                    nuevaFactura.setProductoFactura(this.productosFacturaDeLaFacturaActual); //Asignamos los productos a la factura

                    //Insertamos datos en la tabla Factura de la base de datos
                    modelo.altaFactura(nuevaFactura);
                    Factura facturaInsertada = modelo.getFactura(nuevaFactura);

                    //Insertamos datos en la tabla ProductoFactura de la base de datos
                    for(ProductoFactura pf : productosFacturaDeLaFacturaActual){
                            ProductoFactura pfAux = new ProductoFactura();
                            pfAux.setFactura(facturaInsertada);
                            pfAux.setCantidad(pf.getCantidad());
                            pfAux.setProducto(pf.getProducto());
                            modelo.altaProductoFactura(pfAux);
                    }

                    borrarCamposFacturas1(); //Borramos datos de los campos de producto en la seccion facturas
                    borrarCamposFacturas2(); //Borramos datos de los campos de la factura en la seccion facturas
                    this.facturaActual = new Factura();

                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }
                break;

            case "Listar Factura":
                listarFacturas(modelo.getFacturas());
                break;

            case "Modificar Factura":
                try{
                    //Factura facturaSeleccion = (Factura) vista.listFacturas.getSelectedValue();
                    Factura facturaSeleccion = this.facturaActual;
                    facturaSeleccion.setCodigo(vista.txtCodigoFacturas.getText());
                    facturaSeleccion.setDescuento(Double.parseDouble(String.valueOf(vista.txtDescuentoFacturas.getText())));
                    facturaSeleccion.setPago(Double.parseDouble(String.valueOf(vista.txtPagoFacturas.getText())));
                    facturaSeleccion.setCambio(Double.parseDouble(String.valueOf(vista.txtCambioFacturas.getText())));
                    facturaSeleccion.setPrecioTotal(Double.parseDouble(String.valueOf(vista.txtPrecioFacturas.getText())));


                    facturaSeleccion.setUsuario(this.usuarioLogueado); //Asignamos usuario a la factura
                    facturaSeleccion.setProductoFactura(this.productosFacturaDeLaFacturaActual); //Asignamos los productos a la factura


                    //Modificamps los datos en la tabla Factura de la base de datos
                    modelo.modificarFactura(facturaSeleccion);

                    //Modificamos datos en la tabla ProductoFactura de la base de datos
                    for(ProductoFactura pf : productosFacturaDeLaFacturaActual){
                        modelo.modificarProductoFactura(pf);
                    }

                    borrarCamposFacturas1(); //Borramos datos de los campos de producto en la seccion facturas
                    borrarCamposFacturas2(); //Borramos datos de los campos de la factura en la seccion facturas
                    this.facturaActual = new Factura();

                }catch (NumberFormatException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Introduzca datos validos");
                    ex.printStackTrace();
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Alguno de los campos es nulo");
                    ex.printStackTrace();
                }catch (PersistenceException ex){
                    Util.showWarningAlert("Seleccione la factura a eliminar");
                    ex.printStackTrace();
                }
                break;

            case "Borrar Factura":
                try {
                    Factura facturaBorrada = (Factura) vista.listFacturas.getSelectedValue();
                    modelo.borrarFactura(facturaBorrada);
                    listarFacturas(modelo.getFacturas());
                    borrarCamposFacturas1(); //Borramos datos de los campos de producto en la seccion facturas
                    borrarCamposFacturas2(); //Borramos datos de los campos de la factura en la seccion facturas
                    this.productosFacturaDeLaFacturaActual = new ArrayList<ProductoFactura>();
                }catch (IllegalArgumentException ex){
                    Util.showWarningAlert("Seleccione la factura a eliminar");
                    ex.printStackTrace();
                }
                break;

            /*Usuario*/
            case "Hacer Administrador Usuario":
                try {
                    Boolean existeEnBBDD3 = modelo.existeUsuario(vista.txtUsuarioUsuarios.getText());
                    if (existeEnBBDD3) { //Existe el usuario
                        Usuario usuarioModificar = (Usuario) vista.listUsuarios.getSelectedValue();
                        usuarioModificar.setAdministrador(true);

                        usuarioModificar.cifrar();
                        modelo.modificarUsuario(usuarioModificar);
                        Util.showInfoAlert("El usuario se ha convertido en administrador correctamente");
                    } else {
                        Util.showInfoAlert("El usuario no existe");
                    }
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Seleccione el usuario que desee hacer administrador");
                }
                break;

            case "Listar Usuario":
                listarUsuarios(modelo.getUsuarios());
                break;

            case "Borrar Usuario":
                try {
                    Boolean existeEnBBDD4 = modelo.existeUsuario(vista.txtUsuarioUsuarios.getText());
                    if (existeEnBBDD4) { //Existe el usuario
                        Usuario usuarioBorrado = (Usuario) vista.listUsuarios.getSelectedValue();

                        if (this.usuarioLogueado.getNick().equals(usuarioBorrado.getNick())) {
                            if (Util.confirmationAlert("Borrará su propio usuario")) {
                                modelo.borrarUsuario(usuarioBorrado);
                                listarUsuarios(modelo.getUsuarios());
                                borrarCamposUsuarios();
                                Util.showInfoAlert("Usuario borrado correctamente");
                            }
                        } else {
                            if (Util.confirmationAlert("¿Seguro que desea borrar este usuario?")) {
                                modelo.borrarUsuario(usuarioBorrado);
                                listarUsuarios(modelo.getUsuarios());
                                borrarCamposUsuarios();
                                Util.showInfoAlert("Usuario borrado correctamente");
                            }
                        }
                    } else {
                        Util.showInfoAlert("El usuario no existe");
                    }
                }catch (NullPointerException ex){
                    Util.showWarningAlert("Seleccione el usuario a borrar");
                }
                break;
        }

        //Listo Objetos
        listarProductos(modelo.getProductos());
        listarSecciones(modelo.getSecciones());
        listarFacturas(modelo.getFacturas());
        listarUsuarios(modelo.getUsuarios());
        listarMarcasProducto(modelo.getMarcasDeProductos());
        listarProductosFactura(this.productosFacturaDeLaFacturaActual);

        //Checkboxes -->
        refrescarSeccines();
        refrescarProductos();
    }

    /****************************************************************************************************************/

    /**
     * Función que vacia los campos de la seccion Loguearse
     */
    private void borrarCamposLoguin() {
        vista.loguearse.txtUsuarioLoguearse.setText("");
        vista.loguearse.passwordField1Loguearse.setText("");
    }

    /**
     * Función que vacia los campos de la seccion Registrarse
     */
    private void borrarCamposRegistro() {
        vista.registrarse.txtUsuarioRegistrarse.setText("");
        vista.registrarse.passwordField1Registrarse.setText("");
        vista.registrarse.passwordField2Registrarse.setText("");
        vista.registrarse.txtNombreRegistrarse.setText("");
        vista.registrarse.txtApellidosRegistrarse.setText("");
        vista.registrarse.txtDniRegistrarse.setText("");
        vista.registrarse.txtEmailRegistrarse.setText("");
        vista.registrarse.txtTelefonoRegistrarse.setText("");
    }

    /**
     * Función que vacia los campos de pass de perfil
     */
    private void borrarCamposPerfil() {
        vista.perfil.passwordFieldViejaContrasenaPerfil.setText("");
        vista.perfil.passwordFieldNuevaContrasenaPerfil.setText("");
        vista.perfil.passwordFieldRepetirContrasenaPerfil.setText("");
    }

    /**
     * Función que vacia los campos de la seccion Productos
     */
    private void borrarCamposProductos() {
        vista.txtNombreProductos.setText("");
        vista.txtPrecioProductos.setText("");
        vista.datePickerProductos.setText("");
        vista.txtMarcaProductos.setText("");
        vista.txtDescripcionProductos.setText("");
        vista.comboBoxSeccionProductos.setSelectedItem(-1);
        pegarImagenBase();
    }

    /**
     * Función que vacia los campos de la seccion Secciones
     */
    private void borrarCamposSecciones() {
        vista.txtNombreSecciones.setText("");
        vista.txtDescripcionSecciones.setText("");
        vista.checkBoxHabilitadaSecciones.setSelected(false);
        vista.horaAperturaSecciones.setText("");
        vista.horaCierreSecciones.setText("");
        vista.spinnerEspacioTotalSecciones.setValue(0);
        vista.spinnerEspacioOcupadoSecciones.setValue(0);
    }

    /**
     * Función que vacia los campos de la seccion BuscarProducto
     */
    private void borrarCamposBuscarProductos() {
        vista.comboBoxSeccionBuscarProductos.setSelectedItem(-1);
        vista.comboBoxMarcaBuscarProductos.setSelectedItem(-1);
    }

    /**
     * Función que vacia los campos de la primera seccion Facturas
     */
    private void borrarCamposFacturas1() {
        vista.txtCodigoFacturas.setText("");
        vista.txtDescuentoFacturas.setText("");
        vista.txtPagoFacturas.setText("");
        vista.txtCambioFacturas.setText("");
        vista.txtPrecioFacturas.setText("");
    }
    /**
     * Función que vacia los campos de la segunda seccion Facturas
     */
    private void borrarCamposFacturas2() {
        vista.comboBoxProductoFacturas.setSelectedItem(-1);
        vista.spinnerCantidadFacturas.setValue(0);
        this.productosFacturaDeLaFacturaActual = new ArrayList<ProductoFactura>();
    }

    /**
     * Función que vacia los campos de la seccion Usuarios
     */
    private void borrarCamposUsuarios() {
        vista.txtUsuarioUsuarios.setText("");
        vista.txtNombreUsuarios.setText("");
        vista.txtApellidosUsuarios.setText("");
        vista.txtDniUsuarios.setText("");
        vista.txtEmailUsuarios.setText("");
        vista.txtTelefonoUsuarios.setText("");
        vista.checkBoxAdministradorUsuarios.setSelected(false);
    }

    /****************************************************************************************************************/

    /**
     * Función que añade al dlm de productosPorFactura los productos en la factura con su numero
     * @param lista Lista que contiene los productos y la cantidad de los mismos en una factura
     */
    public void listarProductosFactura(ArrayList<ProductoFactura> lista){
        vista.dlmProductosFactura.clear();
        for(ProductoFactura unProductoFactura : lista){
            vista.dlmProductosFactura.addElement(unProductoFactura);
        }
        this.productosFacturaDeLaFacturaActual = lista;
    }

    /**
     * Función que añade al dlm de buscarProductos los productos de lista
     * @param lista Lista que contiene los productos y la cantidad de los mismos en una factura
     */
    public void listarBuscarProductos(ArrayList<Producto> lista){
        vista.dlmBuscarProductos.clear();
        for(Producto unProducto : lista){
            vista.dlmBuscarProductos.addElement(unProducto);
        }
    }


    /**
     * Función que añade al dlm de productos los productos de lista
     * @param lista Lista que contiene los productos
     */
    public void listarMarcasProducto(ArrayList<String> lista){
        vista.dlmMarcasProductos.clear();
        for(String unaMarca : lista){
            vista.dlmMarcasProductos.addElement(unaMarca);
        }
    }

    /**
     * Función que añade al dlm de productos los productos de lista
     * @param lista Lista que contiene los productos
     */
    public void listarProductos(ArrayList<Producto> lista){
        vista.dlmProductos.clear();
        for(Producto unProducto : lista){
            vista.dlmProductos.addElement(unProducto);
        }
    }

    /**
     * Función que añade al dlm de secciones las secciones de lista
     * @param lista Lista que contiene las secciones
     */
    public void listarSecciones(ArrayList<Seccion> lista){
        vista.dlmSecciones.clear();
        for(Seccion unaSeccion : lista){
            vista.dlmSecciones.addElement(unaSeccion);
        }
    }

    /**
     * Función que añade al dlm de facturas las facturas de lista
     * @param lista Lista que contiene las facturas
     */
    public void listarFacturas(ArrayList<Factura> lista){
        vista.dlmFacturas.clear();
        for(Factura unaFactura : lista){
            vista.dlmFacturas.addElement(unaFactura);
        }
    }

    /**
     * Función que añade al dlm de usuarios los usuarios de lista
     * @param lista Lista que contiene los usuarios
     */
    public void listarUsuarios(ArrayList<Usuario> lista){
        vista.dlmUsuarios.clear();
        for(Usuario unUsuario : lista){
            unUsuario.setCifrado(true);
            vista.dlmUsuarios.addElement(unUsuario);
        }
    }

    /****************************************************************************************************************/

    /**
     * Cambia la imagen en el label de nombre imagenProductos
     * @param imagen Imagen de un producto
     */
    private void cambiarImagenProducto(Image imagen){
        if(imagen != null){
            ImageIcon imageIcon = new ImageIcon(imagen.getScaledInstance(150, 150, Image.SCALE_SMOOTH));
            Icon icon = TrabajarImagenes.imageIconToIcon(imageIcon);
            vista.imagenProductos.setIcon(icon);
        }
    }

    /**
     * Pega la imagen base en la sección imgaen del producto
     */
    private void pegarImagenBase(){
        Image imagen = null;
        try {
            imagen = ImageIO.read(new File("src/com/victorariasbenito/elahorroton/resources/imagenes/ejemploImagen.png"));
            cambiarImagenProducto(imagen);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /****************************************************************************************************************/

    /**
     * Funcion que oculta todos los objetos de la vista excepto los que puede ver un usuario sin registrar
     */
    private void ocultarTodosLosObjetos(){
        /*Producto*/
        vista.altaProductoButton.setVisible(false);
        vista.modificarProductoButton.setVisible(false);
        vista.borrarProductoButton.setVisible(false);

        /*Secciones*/
        vista.altaSeccionesButton.setVisible(false);
        vista.modificarSeccionesButton.setVisible(false);
        vista.borrarSeccionesButton.setVisible(false);

        ocultarAlgunosObjetos();
        /*Secciones enteras ocultas interfaz*/
        vista.tabbedPane1.setEnabledAt(4, false);
    }

    /**
     * Funcion que oculta todos los objetos de la vista excepto los que puede ver un usuario registrado
     */
    private void ocultarAlgunosObjetos(){

        /*Secciones enteras ocultas interfaz*/
        vista.tabbedPane1.setEnabledAt(3, false);

        /*Menus*/
        vista.barra.getComponent(1).setEnabled(false); //Deshabilita el menu perfil

        vista.loguearseItem.getComponent().setEnabled(true); //Habilitamos boton del menu Loguearse
        vista.registrarseItem.getComponent().setEnabled(true); //Habilitamos boton del menu Registrarse
    }

    /**
     * Funcion que muestra todos los objetos bloqueados de la interfaz
     */
    public void mostrarTodosLosObjetos(){
        mostrarAlgunosObjetos();

        /*Producto*/
        vista.altaProductoButton.setVisible(true);
        vista.modificarProductoButton.setVisible(true);
        vista.borrarProductoButton.setVisible(true);

        /*Secciones*/
        vista.altaSeccionesButton.setVisible(true);
        vista.modificarSeccionesButton.setVisible(true);
        vista.borrarSeccionesButton.setVisible(true);

        /*Secciones enteras ocultas interfaz*/
        vista.tabbedPane1.setEnabledAt(4, true);
    }

    /**
     * Funcion que muestra algunos objetos bloqueados de la interfaz
     */
    public void mostrarAlgunosObjetos(){

        /*Secciones enteras ocultas interfaz*/
        vista.tabbedPane1.setEnabledAt(3, true);

        /*Menus*/
        vista.barra.getComponent(1).setEnabled(true); //Habilita el menu perfil

        vista.loguearseItem.getComponent().setEnabled(false); //Deshabilitamos boton del menu Loguearse
        vista.registrarseItem.getComponent().setEnabled(false); //Deshabilitamos boton del menu Registrarse
    }

    /****************************************************************************************************************/

    /**
     * Actualiza las secciones que se ven en la lista y los comboboxes
     */
    private void refrescarSeccines() {
        listarSecciones(modelo.getSecciones());
        vista.comboBoxSeccionProductos.removeAllItems();
        for(int i = 0; i < vista.dlmSecciones.getSize(); i++) {
            Seccion s = (Seccion) vista.dlmSecciones.getElementAt(i);
            vista.comboBoxSeccionProductos.addItem(s);
        }
    }

    /**
     * Actualiza las secciones que se ven en la lista y los comboboxes
     */
    private void refrescarProductos() {
        listarProductos(modelo.getProductos());
        vista.comboBoxProductoFacturas.removeAllItems();
        for(int i = 0; i < vista.dlmProductos.getSize(); i++) {
            vista.comboBoxProductoFacturas.addItem(vista.dlmProductos.getElementAt(i));
        }
    }

    /**
     * Actualiza las secciones que se ven en la lista y los comboboxes de buscarProducto
     */
    private void refrescarSeccinesProducto() {
        listarSecciones(modelo.getSecciones());
        vista.comboBoxSeccionBuscarProductos.removeAllItems();
        for(int i = 0; i < vista.dlmSecciones.getSize(); i++) {
            Seccion s = (Seccion) vista.dlmSecciones.getElementAt(i);
            vista.comboBoxSeccionBuscarProductos.addItem(s);
        }
    }


    /**
     * Actualiza las marcas que se ven en la lista y los comboboxes
     */
    private void refrescarMarcasProductos() {
        listarMarcasProducto(modelo.getMarcasDeProductos());
        vista.comboBoxMarcaBuscarProductos.removeAllItems();
        for(int i = 0; i < vista.dlmMarcasProductos.getSize(); i++) {
            vista.comboBoxMarcaBuscarProductos.addItem(vista.dlmMarcasProductos.getElementAt(i));
        }
    }

    /**
     * Actualiza los productos en una factura que se ven en la lista y los comboboxes
     */
    private void refrescarProductosFacturas(Factura factura) {
        listarProductosFactura(modelo.getProductosPorFactura(factura));
        vista.comboBoxProductoFacturas.removeAllItems();
        for(int i = 0; i < vista.dlmProductos.getSize(); i++) {
            vista.comboBoxProductoFacturas.addItem(vista.dlmProductos.getElementAt(i));
        }
    }


    /****************************************************************************************************************/

    /**
     * Funcion que ejecuta los cambios tras loguearse un usuario
     * @param unUsuario
     */
    private void acctualizarUsuarioRegistrado(Usuario unUsuario){
        this.usuarioLogueado = unUsuario;
        if(unUsuario.isAdministrador()){
            mostrarTodosLosObjetos();
        }else{
            mostrarAlgunosObjetos();
        }
    }

    private void cerrarSesionUsuario(){
        this.usuarioLogueado = null;
        vista.tabbedPane1.setSelectedIndex(0);
        ocultarTodosLosObjetos();
    }

    /****************************************************************************************************************/

    /**
     * Calcula el precio total de una factura y lo escribe en la vista
     * @param productoFacturas Contiene una factura su producto y la cantidad
     */
    private void calcularPrecioFactura(ArrayList<ProductoFactura> productoFacturas){
        Double precioProducto = 0.0;
        Double precioTotal = 0.0;
        int cantidadProductos = 0;

        for(ProductoFactura productoFactura : productoFacturas){
            precioProducto = productoFactura.getProducto().getPrecio();
            cantidadProductos = productoFactura.getCantidad();
            precioTotal += precioProducto * cantidadProductos;
        }
        Double descuento = Double.parseDouble(vista.txtDescuentoFacturas.getText());
        vista.txtPrecioFacturas.setText(String.valueOf(precioTotal-descuento));
    }

    /****************************************************************************************************************/

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listProductos) {
                Producto productoSeleccion = (Producto)vista.listProductos.getSelectedValue();
                vista.txtNombreProductos.setText(productoSeleccion.getNombre());
                vista.txtPrecioProductos.setText(String.valueOf(productoSeleccion.getPrecio()));
                vista.datePickerProductos.setDate(productoSeleccion.getFechaCaducidad().toLocalDate());
                vista.txtMarcaProductos.setText(productoSeleccion.getMarca());
                vista.txtDescripcionProductos.setText(productoSeleccion.getDescripcion());
                vista.comboBoxSeccionProductos.setSelectedItem((Seccion) productoSeleccion.getSeccion()); //Seleccionamos seccion del producto
                //Obtenemos el array de byte y lo cambiamos a Image, despues llamamos a cambiarImagenProducto()
                try {
                    Image imagen = TrabajarImagenes.bytesToImage(productoSeleccion.getImagen());
                    cambiarImagenProducto(imagen);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }else if(e.getSource() == vista.listSecciones){
                Seccion seccionSeleccion = (Seccion)vista.listSecciones.getSelectedValue();
                vista.txtNombreSecciones.setText(seccionSeleccion.getNombre());
                vista.txtDescripcionSecciones.setText(seccionSeleccion.getDescripcion());
                vista.checkBoxHabilitadaSecciones.setSelected(seccionSeleccion.isHabilitada());
                vista.horaAperturaSecciones.setTime(seccionSeleccion.getHoraApertura().toLocalTime());
                vista.horaCierreSecciones.setTime(seccionSeleccion.getHoraCierre().toLocalTime());
                vista.spinnerEspacioTotalSecciones.setValue(seccionSeleccion.getEspacioTotal());
                vista.spinnerEspacioOcupadoSecciones.setValue(seccionSeleccion.getEspacioOcupado());

            }else if(e.getSource() == vista.listFacturas){
                Factura facturaSeleccion = (Factura)vista.listFacturas.getSelectedValue();
                refrescarProductosFacturas(facturaSeleccion); //Obtenemos los productos en la factura
                vista.txtCodigoFacturas.setText(facturaSeleccion.getCodigo());
                vista.txtDescuentoFacturas.setText(String.valueOf(facturaSeleccion.getDescuento()));
                vista.txtPagoFacturas.setText(String.valueOf(facturaSeleccion.getPago()));
                vista.txtCambioFacturas.setText(String.valueOf(facturaSeleccion.getCambio()));
                vista.txtPrecioFacturas.setText(String.valueOf(facturaSeleccion.getPrecioTotal()));
                this.facturaActual = facturaSeleccion;
            }else if(e.getSource() == vista.listProductoFacturas){
                ProductoFactura productoFacturaSeleccion = (ProductoFactura)vista.listProductoFacturas.getSelectedValue();
                vista.comboBoxProductoFacturas.setSelectedItem((Producto) productoFacturaSeleccion.getProducto());
                vista.spinnerCantidadFacturas.setValue(productoFacturaSeleccion.getCantidad());
            }else if(e.getSource() == vista.listUsuarios){
                Usuario usuarioSeleccion = (Usuario) vista.listUsuarios.getSelectedValue();
                usuarioSeleccion.setCifrado(true); //Indicamos que el contenido esta cifrado
                usuarioSeleccion.descifrar();//Desciframos los datos del usuario
                vista.txtUsuarioUsuarios.setText(usuarioSeleccion.getNick());
                vista.txtNombreUsuarios.setText(usuarioSeleccion.getNombre());
                vista.txtApellidosUsuarios.setText(usuarioSeleccion.getApellidos());
                vista.txtDniUsuarios.setText(usuarioSeleccion.getDni());
                vista.txtEmailUsuarios.setText(usuarioSeleccion.getEmail());
                vista.txtTelefonoUsuarios.setText(usuarioSeleccion.getTelefono());
                vista.checkBoxAdministradorUsuarios.setSelected(usuarioSeleccion.isAdministrador());
                usuarioSeleccion.cifrar(); //Ciframos los datos del usuario
            }
        }
    }

    /****************************************************************************************************************/

    @Override
    public void mouseClicked(MouseEvent e) {
        if(e.getSource() == vista.imagenProductos) {
            Image imagen = null;
            try {
                imagen = Util.showJFileChooser();
                cambiarImagenProducto(imagen);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /****************************************************************************************************************/

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
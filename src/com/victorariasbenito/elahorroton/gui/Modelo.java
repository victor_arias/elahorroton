package com.victorariasbenito.elahorroton.gui;

import com.victorariasbenito.elahorroton.clases.*;
import com.victorariasbenito.elahorroton.clases.*;
import com.victorariasbenito.elahorroton.libreriasFunciones.Cifrado;
import com.victorariasbenito.elahorroton.libreriasFunciones.JasperReport;
import com.victorariasbenito.elahorroton.util.Util;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import javax.persistence.Query;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

public class Modelo {
    SessionFactory sessionFactory;
    String userBBDD = "root";
    String passBBDD = "";
    Connection conexion;


    /**
     * Lee el archivo de propiedades y setea los atributos pertinentes
     */
    void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            this.userBBDD = prop.getProperty("user");
            this.passBBDD = prop.getProperty("pass");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Devuelve los valores de conexión a base de datos al estado original
     */
    void setValuesDefault(){
        setPropValues("root","");
    }

    /**
     * Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     *
     * @param user user de la bbdd
     * @param pass contraseña de la bbdd
     */
    void setPropValues(String user, String pass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            Util.showErrorAlert("Algo fallo al configurar usuario y contraseña");
            ex.printStackTrace();
        }
        this.userBBDD = user;
        this.passBBDD = pass;
    }

    /**
     * Función que desconecta la base de datos de hibernate
     */
    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Función para conectarse a la base de datos con hibernate
     */
    public void conectar() {

        /*Actualizo valores de conexion*/
        getPropValues();

        try {
            crearBaseDatos();
        } catch (IOException e) {
            Util.showErrorAlert("Algo falló el programa no funcionará correctamete, reinicielo");
            e.printStackTrace();
        }

        Configuration configuracion = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        configuracion.setProperty(Environment.USER,this.userBBDD);
        configuracion.setProperty(Environment.PASS,this.passBBDD);

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Usuario.class);
        configuracion.addAnnotatedClass(Seccion.class);
        configuracion.addAnnotatedClass(Producto.class);
        configuracion.addAnnotatedClass(Factura.class);
        configuracion.addAnnotatedClass(ProductoFactura.class);


        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);
    }

    /**
     * Función que crea la base de datos en caso de que no este creada
     * @throws IOException
     */
    private void crearBaseDatos() throws IOException {
        String ip = "localhost";
        try {
            this.conexion = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/elahorroton",userBBDD, passBBDD);
        } catch (SQLException sqle) {

            try {
                this.conexion = DriverManager.getConnection(
                        "jdbc:mysql://" + ip + ":3306/", userBBDD, passBBDD);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

                Util.showInfoAlert("Se ha creado la base de datos");
                this.conexion = conexion;

            } catch (SQLException e) {
                Util.showErrorAlert("Fallo de conexión a la base de datos, el programa no funcionará\n" +
                        "Si desea que funcione establezca un usuario y contraseña válidos en (Opciones de Conexión)\n" +
                        "Y a continuación clice en Conectar");
                e.printStackTrace();
            }
        }
    }

    /**
     * Funcion que devuelve un String con el texto de basedatos_java.sql
     * @return String
     * @throws IOException
     */
    private String leerFichero() throws IOException {
            BufferedReader reader = new BufferedReader(new FileReader("src/com/victorariasbenito/elahorroton/resources/bbdd/basedatos_java.sql"));
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
    }

    /*Usuario*/

    /**
     * Función que da de alta a un usuario en la base de datos
     * @param nuevoUsuario Usuario que se dará de alta
     */
    public void altaUsuario(Usuario nuevoUsuario) {
        //Obtengo una session a partir de la seccionFactory
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoUsuario);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Función que obtiene un Array con todos los usuarios de la base de datos
     * @return ArrayList Usuarios
     */
    public ArrayList<Usuario> getUsuarios() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Usuario");
        ArrayList<Usuario> lista = (ArrayList<Usuario>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Funciónn que actualiza un usuario en la base de datos
     * @param usuarioSeleccion El usuario a modificar con las modificaciones
     */
    public void modificarUsuario(Usuario usuarioSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(usuarioSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Función que borra un usuario en la base de datos
     * @param usuarioBorrado El usuario que se va a borrar
     */
    public void borrarUsuario(Usuario usuarioBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(usuarioBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Función que devuelve si existe el usuario o no en la base de datos
     * @param nombreUsuario Dato String con el nombre del usuario del que se quiere comprobar la existencia
     * @return boolean true si existe el usuario y false en caso contrario
     */
    public boolean existeUsuario(String nombreUsuario) {
        String nombreCifrado = "";
        try {
            nombreCifrado = Cifrado.cifrar(nombreUsuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Usuario WHERE nick = :mrc");
        query.setParameter("mrc", nombreCifrado);
        ArrayList<Usuario> lista = (ArrayList<Usuario>) query.getResultList();
        sesion.close();
        if(lista.size() > 0){
            return true;
        }
        return false;
    }

    /**
     * Función que devuelve un Usuario de la base de datos
     * @param nombreUsuario String con el nombre del usuario
     * @return Un dato tipo Usuario
     */
    public Usuario getUsuario(String nombreUsuario) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Usuario WHERE nick = :mrc");
        query.setParameter("mrc", nombreUsuario);
        ArrayList<Usuario> lista = (ArrayList<Usuario>) query.getResultList();
        sesion.close();
        if(lista.size() > 0){
            return lista.get(0);
        }
        return null;
    }

    /**
     * Función que es true si el dni existe y false en caso contrario
     * @param dni String con el dni del usuario
     * @return Boolean true si el dni existe y false en caso contrario
     */
    public Boolean existeDni(String dni) {
        String dniCifrado = "";
        try {
            dniCifrado = Cifrado.cifrar(dni);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Usuario WHERE dni = :mrc");
        query.setParameter("mrc", dniCifrado);
        ArrayList<Usuario> lista = (ArrayList<Usuario>) query.getResultList();
        sesion.close();
        if(lista.size() > 0){
            return true;
        }
        return false;
    }

    /*Seccion*/

    /**
     * Función que da de alta una nueva sección en la base de datos
     * @param nuevaSeccion Dato tipo Sección que se va a dar de alta
     */
    public void altaSeccion(Seccion nuevaSeccion) {
        //Obtengo una session a partir de la sessionFactory
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaSeccion);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Función que obtiene todas las Secciones de la base de datos
     * @return ArrayList Seccion
     */
    public ArrayList<Seccion> getSecciones() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Seccion");
        ArrayList<Seccion> lista = (ArrayList<Seccion>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Función que modifica una Sección de la base de datos
     * @param seccionSeleccion Seccion modificada
     */
    public void modificarSeccion(Seccion seccionSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(seccionSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Función que borra una Sección de la base de datos
     * @param seccionBorrada Sección que se va a borrar
     */
    public void borrarSeccion(Seccion seccionBorrada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(seccionBorrada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /*Producto*/

    /**
     * Función que da de alta un dato Producto en la base de datos
     * @param nuevoProducto Producto que se va a introducir en la base de datos
     */
    public void altaProducto(Producto nuevoProducto) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoProducto);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Función que obtiene todos los productos de la base de datos
     * @return ArrayList Producto con todos los productos de la base de datos
     */
    public ArrayList<Producto> getProductos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto");
        ArrayList<Producto> lista = (ArrayList<Producto>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Función que modifica un Producto en la base de datos
     * @param productoSeleccion Dato tipo Producto modificado
     */
    public void modificarProducto(Producto productoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(productoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Función que borra los Productos de la base de datos
     * @param productoBorrado Dato Producto a borrar de la base de datos
     */
    public void borrarProducto(Producto productoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(productoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /******************************************************************************/

    /**
     * Función que devuelve todas las marcas de los productos de la base de datos
     * @return ArrayList String con las marcas de los productos
     */
    public ArrayList<String> getMarcasDeProductos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("SELECT DISTINCT marca FROM Producto");
        ArrayList<String> lista = (ArrayList<String>) query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Función que devuelve todos los Productos de una marca de la base de datos
     * @param marcaSeleccionada String con la marca de productos seleccionada
     * @return ArrayList Producto Con los productos de esa marca
     */
    public ArrayList<Producto> getProductosPorMarca(String marcaSeleccionada) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto WHERE marca = :mrc");
        query.setParameter("mrc", marcaSeleccionada);
        ArrayList<Producto> lista = (ArrayList<Producto>) query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Función que develve todos los productos dentro de una seccion en la base de datos
     * @param seccionSeleccionada Dato tipo Seccion que indica la sección de los productos a buscar
     * @return ArrayList Productos Todos los productos de una seccion
     */
    public ArrayList<Producto> getProductosPorSeccion(Seccion seccionSeleccionada) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Producto WHERE seccion = :mrc");
        query.setParameter("mrc", seccionSeleccionada);
        ArrayList<Producto> lista = (ArrayList<Producto>) query.getResultList();
        sesion.close();
        return lista;
    }

    /*Factura*/

    /**
     * Función que da de alta una Factura en la base de datos
     * @param nuevaFactura Dato tipo Factura a introducir en la base de datos
     */
    public void altaFactura(Factura nuevaFactura) {
        //Obtengo una session a partir de la sessionFactory
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaFactura);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Función que obtiene todas las facturas de la base de datos
     * @return ArrayList Factura con todas las facturas
     */
    public ArrayList<Factura> getFacturas() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Factura");
        ArrayList<Factura> lista = (ArrayList<Factura>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Función que modifica una factura en la base de datos
     * @param facturaSeleccion Dato tipo Factura modificado
     */
    public void modificarFactura(Factura facturaSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(facturaSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Función que borra la factura seleccionada de la base de datos
     * @param facturaBorrada Dato tipo Factura que se borrará
     */
    public void borrarFactura(Factura facturaBorrada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(facturaBorrada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /******************************************************************************/

    /**
     * Función que devuelve todas las facturas creadas por un usuario de la base de datos
     * @param usuarioSeleccionado Dato tipo Usaurio del que se obtendran las facturas
     * @return ArrayList Factura Con las facturas del usuario
     */
    public ArrayList<Factura> getFacturasPorUsuario(Usuario usuarioSeleccionado) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Factura WHERE usuario = :mrc");
        query.setParameter("mrc", usuarioSeleccionado);
        ArrayList<Factura> lista = (ArrayList<Factura>) query.getResultList();
        sesion.close();
        return lista;
    }


    /*ProductoFactura*/

    /**
     * Función que da de alta un ProductoFactura de la base de datos
     * @param nuevoProductoFactura Dato ProductoFactura que se va a dar de alta
     */
    public void altaProductoFactura(ProductoFactura nuevoProductoFactura) {
        //Obtengo una session a partir de la sessionFactory
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoProductoFactura);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Función que modifica un ProdutoFactura de la base de datos
     * @param productoFacturaSeleccion Dato tipo ProductoFactura modificado
     */
    public void modificarProductoFactura(ProductoFactura productoFacturaSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(productoFacturaSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Función que elimina un ProductoFactura de la base de datos
     * @param productoFacturaBorrado ProductoFactura que se va a eliminar
     */
    public void borrarProductoFactura(ProductoFactura productoFacturaBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(productoFacturaBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /******************************************************************************/

    /**
     * Función que obtiene los ProductoFactura de una factura
     * @param facturaSeleccionada La factura de la que se quieren obtener los datos
     * @return ArrayList ProductoFactura con los datos ProductoFactura de la Factura
     */
    public ArrayList<ProductoFactura> getProductosPorFactura(Factura facturaSeleccionada) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM ProductoFactura WHERE factura = :mrc");
        query.setParameter("mrc", facturaSeleccionada);
        ArrayList<ProductoFactura> lista = (ArrayList<ProductoFactura>) query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Función que obitene la última factura insertada en la base de datos
     * @param nuevaFactura Ultima factura insertada
     * @return Factura insertada
     */
    public Factura getFactura(Factura nuevaFactura) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Factura WHERE id = (SELECT max(id) FROM Factura) AND codigo = :mrc");
        query.setParameter("mrc", nuevaFactura.getCodigo());
        ArrayList<Factura> lista = (ArrayList<Factura>) query.getResultList();
        sesion.close();
        for (Factura f : lista){
            return f;
        }
        return null;
    }

    /******************************************************************************************************************/

    /**
     * Función que en generera un pdf y lo muestra por pantalla
     * @param direccionFicheroOriginal Ubicación del fichero.jasper necesario
     * @param direccionNuevoFicheroPDF Ubicación para guardar el pdf generado, si es null o vacio no se guardará el pdf
     */
    public void generarInformeJasperSoft(String direccionFicheroOriginal, String direccionNuevoFicheroPDF){
        JasperReport.generarInformePrincipal(this.conexion, direccionFicheroOriginal, direccionNuevoFicheroPDF);
    }

}

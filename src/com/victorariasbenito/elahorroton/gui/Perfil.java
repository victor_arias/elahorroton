package com.victorariasbenito.elahorroton.gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Perfil extends JDialog {
    private JPanel panel1;
    private Frame owner;

    JTextField txtUsuarioPerfil;
    JTextField txtNombrePerfil;
    JTextField txtApellidosPerfil;
    JTextField txtDniPerfil;
    JTextField txtEmailPerfil;
    JTextField txtTelefonoPerfil;
    JButton modificarPerfilButton;
    JButton eliminarPerfilButton;
    JSeparator separadorPerfil;
    JPasswordField passwordFieldViejaContrasenaPerfil;
    JPasswordField passwordFieldNuevaContrasenaPerfil;
    JPasswordField passwordFieldRepetirContrasenaPerfil;
    JButton cambiarContrasenaButton;
    JCheckBox checkBoxAdministradorPerfil;
    JLabel lb1;
    JLabel lb2;
    JLabel lb3;
    JLabel lb4;
    JLabel lb5;
    JLabel lb6;
    JLabel lb7;
    JLabel lb8;
    JLabel lb9;
    JLabel lb10;

    /**
     * Constructor de la clase.
     * @param owner propietario del dialog
     */
    public Perfil(Frame owner) {
        super(owner, "Perfil", true);
        this.owner = owner;

        //Image icon = new ImageIcon(getClass().getResource("src/com/victorariasbenito/elahorroton/base/resources/imagenes/ejemploImagen.png")).getImage();
        Image icon = null;
        try {
            icon = ImageIO.read(new File("src/com/victorariasbenito/elahorroton/resources/imagenes/icono.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.owner.setIconImage(icon);

        initDialog();
        aumentarFuenteComponentes();
    }

    /**
     * Inicializa el JDialog
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        //this.setBounds(300,200,500,500);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+100, this.getHeight()+100));
        this.setLocationRelativeTo(owner);
    }
    /**
     * Aumenta el tamaño de los componentes
     */
    private void aumentarFuenteComponentes() {
        Vista.aumentarFuenteComponent(txtUsuarioPerfil);
        Vista.aumentarFuenteComponent(txtNombrePerfil);
        Vista.aumentarFuenteComponent(txtApellidosPerfil);
        Vista.aumentarFuenteComponent(txtDniPerfil);
        Vista.aumentarFuenteComponent(txtEmailPerfil);
        Vista.aumentarFuenteComponent(txtTelefonoPerfil);
        Vista.aumentarFuenteComponent(modificarPerfilButton);
        Vista.aumentarFuenteComponent(eliminarPerfilButton);
        Vista.aumentarFuenteComponent(separadorPerfil);
        Vista.aumentarFuenteComponent(passwordFieldViejaContrasenaPerfil);
        Vista.aumentarFuenteComponent(passwordFieldNuevaContrasenaPerfil);
        Vista.aumentarFuenteComponent(passwordFieldRepetirContrasenaPerfil);
        Vista.aumentarFuenteComponent(cambiarContrasenaButton);
        Vista.aumentarFuenteComponent(checkBoxAdministradorPerfil);
        Vista.aumentarFuenteComponent(lb1);
        Vista.aumentarFuenteComponent(lb2);
        Vista.aumentarFuenteComponent(lb3);
        Vista.aumentarFuenteComponent(lb4);
        Vista.aumentarFuenteComponent(lb5);
        Vista.aumentarFuenteComponent(lb6);
        Vista.aumentarFuenteComponent(lb7);
        Vista.aumentarFuenteComponent(lb8);
        Vista.aumentarFuenteComponent(lb9);
        Vista.aumentarFuenteComponent(lb10);
    }
}

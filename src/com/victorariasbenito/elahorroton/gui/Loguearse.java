package com.victorariasbenito.elahorroton.gui;


import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 *
 */
public class Loguearse extends JDialog {
    private JPanel panel1;
    private Frame owner;

    JTextField txtUsuarioLoguearse;
    JPasswordField passwordField1Loguearse;
    JButton loginButtonLoguearse;
    JLabel lb1;
    JLabel lb2;

    /**
     * Constructor de la clase.
     * @param owner propietario del dialog
     */
    public Loguearse(Frame owner) {
        super(owner, "Loguearse", true);
        this.owner = owner;

        //Image icon = new ImageIcon(getClass().getResource("src/com/victorariasbenito/elahorroton/base/resources/imagenes/ejemploImagen.png")).getImage();
        Image icon = null;
        try {
            icon = ImageIO.read(new File("src/com/victorariasbenito/elahorroton/resources/imagenes/icono.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.owner.setIconImage(icon);

        initDialog();
        aumentarFuenteComponentes();
    }

    /**
     * Inicializa el JDialog
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+10, this.getHeight()+10));
        this.setLocationRelativeTo(owner);
    }

    /**
     * Aumenta el tamaño de los componentes
     */
    private void aumentarFuenteComponentes() {
        Vista.aumentarFuenteComponent(txtUsuarioLoguearse);
        Vista.aumentarFuenteComponent(passwordField1Loguearse);
        Vista.aumentarFuenteComponent(loginButtonLoguearse);
        Vista.aumentarFuenteComponent(lb1);
        Vista.aumentarFuenteComponent(lb2);
    }
}
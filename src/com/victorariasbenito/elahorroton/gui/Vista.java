package com.victorariasbenito.elahorroton.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.TimePicker;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Vista extends JFrame{
    JFrame frame;
    JPanel panel1;
    JTabbedPane tabbedPane1;
    JMenuBar barra;

    /*Productos*/
    JPanel jpProductos;
    JTextField txtNombreProductos;
    JTextField txtPrecioProductos;
    DatePicker datePickerProductos;
    JTextField txtMarcaProductos;
    JTextField txtDescripcionProductos;
    JComboBox comboBoxSeccionProductos;
    JLabel imagenProductos;
    JSeparator separadorProductos;
    JButton listarProductosButton;
    JButton altaProductoButton;
    JButton modificarProductoButton;
    JButton borrarProductoButton;
    JList listProductos;
    JLabel l10;
    JLabel l11;
    JLabel l12;
    JLabel l13;
    JLabel l14;
    JLabel l15;
    JLabel l16;

    /*Secciones*/
    JPanel jpSecciones;
    JTextField txtNombreSecciones;
    JTextField txtDescripcionSecciones;
    JCheckBox checkBoxHabilitadaSecciones;
    TimePicker horaAperturaSecciones;
    TimePicker horaCierreSecciones;
    JSpinner spinnerEspacioTotalSecciones;
    JSpinner spinnerEspacioOcupadoSecciones;
    JSeparator separadorSecciones;
    JButton listarSeccionesButton;
    JButton altaSeccionesButton;
    JButton modificarSeccionesButton;
    JButton borrarSeccionesButton;
    JList listSecciones;
    JLabel l1;
    JLabel l2;
    JLabel l3;
    JLabel l4;
    JLabel l5;
    JLabel l6;
    JLabel l7;
    JLabel l8;
    JLabel l9;

    /*Productos por seccion*/
    JPanel jpBuscarProductos;
    JComboBox comboBoxSeccionBuscarProductos;
    JComboBox comboBoxMarcaBuscarProductos;
    JSeparator separadorBuscarProductos;
    JButton buscarPorSeccionButton;
    JButton buscarPorMarcaButton;
    JButton pdfProductosPorSeccionButton;
    JList listBuscarProductos;
    JList listMarcasProductos;
    JLabel l17;
    JLabel l18;
    JLabel l19;

    /*Facturas*/
    JPanel jpFacturas;
    JTextField txtCodigoFacturas;
    JTextField txtDescuentoFacturas;
    JTextField txtPagoFacturas;
    JTextField txtCambioFacturas;
    JTextField txtPrecioFacturas;
    JComboBox comboBoxProductoFacturas;
    JSpinner spinnerCantidadFacturas;
    JButton anadirALaFacturaButton;
    JButton modificarProductoFacturaButton;
    JButton eliminarProductoFacturaButton;
    JButton listarFacturasButton;
    JButton borrarFacturaButton;
    JButton altaFacturaButton;
    JButton modificarFacturaButton;
    JSeparator separadorFacturas;
    JList listProductoFacturas;
    JList listFacturas;
    JLabel l20;
    JLabel l21;
    JLabel l22;
    JLabel l23;
    JLabel l24;
    JLabel l25;
    JLabel l26;
    JLabel l27;
    JLabel l28;

    /*Usuarios*/
    JPanel jpUsuarios;
    JTextField txtUsuarioUsuarios;
    JTextField txtNombreUsuarios;
    JTextField txtApellidosUsuarios;
    JTextField txtDniUsuarios;
    JTextField txtEmailUsuarios;
    JTextField txtTelefonoUsuarios;
    JCheckBox checkBoxAdministradorUsuarios;
    JButton listarUsuariosButton;
    JButton hacerAdministradorUsuarioButton;
    JButton borrarUsuarioButton;
    JList listUsuarios;
    JLabel l29;
    JLabel l30;
    JLabel l31;
    JLabel l32;
    JLabel l33;
    JLabel l34;
    JLabel l35;
    JLabel l36;

    /*Default list models*/
    DefaultListModel dlmUsuarios;
    DefaultListModel dlmSecciones;
    DefaultListModel dlmProductos;
    DefaultListModel dlmBuscarProductos;
    DefaultListModel dlmFacturas;
    DefaultListModel dlmProductosFactura;
    DefaultListModel dlmMarcasProductos;

    /*Items del menu*/
    JMenuItem conexionItem;
    JMenuItem opcionesItem;
    JMenuItem loguearseItem;
    JMenuItem registrarseItem;
    JMenuItem informacionItem;
    JMenuItem salirItem;

    JMenuItem perfilItem;
    JMenuItem cerrarSesionItem;

    /*OPTION DIALOGS*/
    OpcionesBbdd opcionesBbdd;
    Loguearse loguearse;
    Registrarse registrarse;
    Perfil perfil;

    /**
     * Constructor de la vista
     */
    public Vista(){

        frame = new JFrame("El Ahorroton");
        crearMenu();
        crearModelos();

        //Image icon = new ImageIcon(getClass().getResource("src/com/victorariasbenito/elahorroton/base/resources/imagenes/ejemploImagen.png")).getImage();
        Image icon = null;
        try {
            icon = ImageIO.read(new File("src/com/victorariasbenito/elahorroton/resources/imagenes/icono.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        frame.setIconImage(icon);

        frame.setBounds(300,200,1280,720);
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.pack();
        frame.setVisible(true);
        //frame.setSize(new Dimension(this.getWidth()+300, this.getHeight()+200));
        frame.setLocationRelativeTo(null);

        //Pantalla de configuración bbdd
        this.opcionesBbdd = new OpcionesBbdd(this);
        this.opcionesBbdd = new OpcionesBbdd(this);
        //Pantalla de administración del perfil
        this.perfil = new Perfil(this);
        //Pantalla para registrarse
        this.registrarse = new Registrarse(this);
        //Pantalla para loguearse
        this.loguearse = new Loguearse(this);

        aumentarFuenteComponentes();
    }

    /**
     *
     */
    private void crearModelos() {
        dlmProductos = new DefaultListModel();
        listProductos.setModel(dlmProductos);

        dlmSecciones = new DefaultListModel();
        listSecciones.setModel(dlmSecciones);

        dlmBuscarProductos = new DefaultListModel();
        listBuscarProductos.setModel(dlmBuscarProductos);

        dlmUsuarios = new DefaultListModel();
        listUsuarios.setModel(dlmUsuarios);

        dlmFacturas = new DefaultListModel();
        listFacturas.setModel(dlmFacturas);

        dlmProductosFactura = new DefaultListModel();
        listProductoFacturas.setModel(dlmProductosFactura);

        listMarcasProductos = new JList();
        dlmMarcasProductos = new DefaultListModel();
        listMarcasProductos.setModel(dlmMarcasProductos);
    }

    /**
     *
     */
    private void crearMenu() {
        Font font = new Font("", Font.PLAIN, 20);

        barra = new JMenuBar();

        /*********************************************************/
        JMenu menu = new JMenu("Archivo");
        menu.setFont(font);

        menu.setBackground(new Color(255, 255, 255));
        menu.setForeground(new Color(115,4,1));

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");
        opcionesItem = new JMenuItem("Opciones de Conexion");
        opcionesItem.setActionCommand("OpcionesConexion");
        loguearseItem = new JMenuItem("Loguearse");
        loguearseItem.setActionCommand("Loguearse");
        registrarseItem = new JMenuItem("Registrarse");
        registrarseItem.setActionCommand("Registrarse");
        informacionItem = new JMenuItem("Informacion");
        informacionItem.setActionCommand("Informacion");
        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        conexionItem.setBackground(new Color(185,51,47));
        opcionesItem.setBackground(new Color(185,51,47));
        loguearseItem.setBackground(new Color(185,51,47));
        registrarseItem.setBackground(new Color(185,51,47));
        informacionItem.setBackground(new Color(185,51,47));
        salirItem.setBackground(new Color(185,51,47));

        conexionItem.setForeground(new Color(86,24,22));
        opcionesItem.setForeground(new Color(86,24,22));
        loguearseItem.setForeground(new Color(86,24,22));
        registrarseItem.setForeground(new Color(86,24,22));
        informacionItem.setForeground(new Color(86,24,22));
        salirItem.setForeground(new Color(86,24,22));

        conexionItem.setFont(font);
        opcionesItem.setFont(font);
        loguearseItem.setFont(font);
        registrarseItem.setFont(font);
        informacionItem.setFont(font);
        salirItem.setFont(font);

        menu.add(conexionItem);
        menu.add(opcionesItem);
        menu.add(loguearseItem);
        menu.add(registrarseItem);
        menu.add(informacionItem);
        menu.add(salirItem);


        barra.add(menu);

        /*********************************************************/
        JMenu menu2 = new JMenu("Perfil");
        menu2.setFont(font);

        menu2.setBackground(new Color(255, 255, 255));
        menu2.setForeground(new Color(115,4,1));

        perfilItem = new JMenuItem("Editar");
        perfilItem.setActionCommand("EditarPerfil");
        cerrarSesionItem = new JMenuItem("Cerrar Sesion");
        cerrarSesionItem.setActionCommand("Cerrar Sesion");

        perfilItem.setFont(font);
        cerrarSesionItem.setFont(font);

        perfilItem.setBackground(new Color(185,51,47));
        cerrarSesionItem.setBackground(new Color(185,51,47));
        perfilItem.setForeground(new Color(86,24,22));
        cerrarSesionItem.setForeground(new Color(86,24,22));

        menu2.add(perfilItem);
        menu2.add(cerrarSesionItem);

        barra.add(menu2);

        /*********************************************************/
        frame.setJMenuBar(barra);
    }


    /**
     * Aumenta el tamaño de los componentes
     */
    private void aumentarFuenteComponentes(){
        aumentarFuenteComponent(panel1);
        aumentarFuenteComponent(tabbedPane1);
        aumentarFuenteComponent(barra);

        /*Productos*/
        aumentarFuenteComponent(jpProductos);
        aumentarFuenteComponent(txtNombreProductos);
        aumentarFuenteComponent(txtPrecioProductos);
        aumentarFuenteComponent(datePickerProductos);
        aumentarFuenteComponent(txtMarcaProductos);
        aumentarFuenteComponent(txtDescripcionProductos);
        aumentarFuenteComponent(comboBoxSeccionProductos);
        aumentarFuenteComponent(imagenProductos);
        aumentarFuenteComponent(separadorProductos);
        aumentarFuenteComponent(listarProductosButton);
        aumentarFuenteComponent(altaProductoButton);
        aumentarFuenteComponent(modificarProductoButton);
        aumentarFuenteComponent(borrarProductoButton);
        aumentarFuenteComponent(listProductos);

        /*Secciones*/
        aumentarFuenteComponent(jpSecciones);
        aumentarFuenteComponent(txtNombreSecciones);
        aumentarFuenteComponent(txtDescripcionSecciones);
        aumentarFuenteComponent(checkBoxHabilitadaSecciones);
        aumentarFuenteComponent(horaAperturaSecciones);
        aumentarFuenteComponent(horaCierreSecciones);
        aumentarFuenteComponent(spinnerEspacioTotalSecciones);
        aumentarFuenteComponent(spinnerEspacioOcupadoSecciones);
        aumentarFuenteComponent(separadorSecciones);
        aumentarFuenteComponent(listarSeccionesButton);
        aumentarFuenteComponent(altaSeccionesButton);
        aumentarFuenteComponent(modificarSeccionesButton);
        aumentarFuenteComponent(borrarSeccionesButton);
        aumentarFuenteComponent(listSecciones);

        /*Productos por seccion*/
        aumentarFuenteComponent(jpBuscarProductos);
        aumentarFuenteComponent(comboBoxSeccionBuscarProductos);
        aumentarFuenteComponent(comboBoxMarcaBuscarProductos);
        aumentarFuenteComponent(separadorBuscarProductos);
        aumentarFuenteComponent(buscarPorSeccionButton);
        aumentarFuenteComponent(buscarPorMarcaButton);
        aumentarFuenteComponent(pdfProductosPorSeccionButton);
        aumentarFuenteComponent(listBuscarProductos);
        aumentarFuenteComponent(listMarcasProductos);

        /*Facturas*/
        aumentarFuenteComponent(jpFacturas);
        aumentarFuenteComponent(txtCodigoFacturas);
        aumentarFuenteComponent(txtDescuentoFacturas);
        aumentarFuenteComponent(txtPagoFacturas);
        aumentarFuenteComponent(txtCambioFacturas);
        aumentarFuenteComponent(txtPrecioFacturas);
        aumentarFuenteComponent(comboBoxProductoFacturas);
        aumentarFuenteComponent(spinnerCantidadFacturas);
        aumentarFuenteComponent(anadirALaFacturaButton);
        aumentarFuenteComponent(modificarProductoFacturaButton);
        aumentarFuenteComponent(eliminarProductoFacturaButton);
        aumentarFuenteComponent(listarFacturasButton);
        aumentarFuenteComponent(borrarFacturaButton);
        aumentarFuenteComponent(altaFacturaButton);
        aumentarFuenteComponent(modificarFacturaButton);
        aumentarFuenteComponent(separadorFacturas);
        aumentarFuenteComponent(listProductoFacturas);
        aumentarFuenteComponent(listFacturas);

        /*Usuarios*/
        aumentarFuenteComponent(jpUsuarios);
        aumentarFuenteComponent(txtUsuarioUsuarios);
        aumentarFuenteComponent(txtNombreUsuarios);
        aumentarFuenteComponent(txtApellidosUsuarios);
        aumentarFuenteComponent(txtDniUsuarios);
        aumentarFuenteComponent(txtEmailUsuarios);
        aumentarFuenteComponent(txtTelefonoUsuarios);
        aumentarFuenteComponent(checkBoxAdministradorUsuarios);
        aumentarFuenteComponent(listarUsuariosButton);
        aumentarFuenteComponent(hacerAdministradorUsuarioButton);
        aumentarFuenteComponent(borrarUsuarioButton);
        aumentarFuenteComponent(listUsuarios);


        /*Labels*/
        aumentarFuenteJLabel(l1);
        aumentarFuenteJLabel(l2);
        aumentarFuenteJLabel(l3);
        aumentarFuenteJLabel(l4);
        aumentarFuenteJLabel(l5);
        aumentarFuenteJLabel(l6);
        aumentarFuenteJLabel(l7);
        aumentarFuenteJLabel(l8);
        aumentarFuenteJLabel(l9);
        aumentarFuenteJLabel(l10);
        aumentarFuenteJLabel(l11);
        aumentarFuenteJLabel(l12);
        aumentarFuenteJLabel(l13);
        aumentarFuenteJLabel(l14);
        aumentarFuenteJLabel(l15);
        aumentarFuenteJLabel(l16);
        aumentarFuenteJLabel(l17);
        aumentarFuenteJLabel(l18);
        aumentarFuenteJLabel(l19);
        aumentarFuenteJLabel(l20);
        aumentarFuenteJLabel(l21);
        aumentarFuenteJLabel(l22);
        aumentarFuenteJLabel(l23);
        aumentarFuenteJLabel(l24);
        aumentarFuenteJLabel(l25);
        aumentarFuenteJLabel(l26);
        aumentarFuenteJLabel(l27);
        aumentarFuenteJLabel(l28);
        aumentarFuenteJLabel(l29);
        aumentarFuenteJLabel(l30);
        aumentarFuenteJLabel(l31);
        aumentarFuenteJLabel(l32);
        aumentarFuenteJLabel(l33);
        aumentarFuenteJLabel(l34);
        aumentarFuenteJLabel(l35);
        aumentarFuenteJLabel(l36);
    }

    /**
     * Aumenta la fuente de un JComponent
     * @param label
     */
    public static void aumentarFuenteComponent(JComponent label){
        Font labelFont = label.getFont();

        // Find out how much the font can grow in width.
        double widthRatio = 1.5;

        int newFontSize = (int)(labelFont.getSize() * widthRatio);
        int componentHeight = label.getHeight();

        // Pick a new font size so it will not be larger than the height of label.
        int fontSizeToUse = Math.min(newFontSize, componentHeight);

        // Set the label's font size to the newly determined size.
        label.setFont(new Font(labelFont.getName(), Font.PLAIN, fontSizeToUse));
    }

    /**
     * Aumenta la fuente de un JComponent
     * @param label
     * @param nuevoTamanyo Tamaño que le va a dar al JComponent
     */
    public static void aumentarFuente(JComponent label, Double nuevoTamanyo){
        Font labelFont = label.getFont();

        // Find out how much the font can grow in width.
        double widthRatio = nuevoTamanyo;

        int newFontSize = (int)(labelFont.getSize() * widthRatio);
        int componentHeight = label.getHeight();

         // Pick a new font size so it will not be larger than the height of label.
        int fontSizeToUse = Math.min(newFontSize, componentHeight);

        // Set the label's font size to the newly determined size.
        label.setFont(new Font(labelFont.getName(), Font.PLAIN, fontSizeToUse));
    }


    /**
     * Aumenta la fuente de un JLabel
     * @param label
     */
    public static void aumentarFuenteJLabel(JLabel label){
        Font labelFont = label.getFont();
        String labelText = label.getText();

        int stringWidth = label.getFontMetrics(labelFont).stringWidth(labelText);
        int componentWidth = label.getWidth();

        // Find out how much the font can grow in width.
        //double widthRatio = (double)componentWidth / (double)stringWidth;
        double widthRatio = 1.5;

        int newFontSize = (int)(labelFont.getSize() * widthRatio);
        int componentHeight = label.getHeight();

        // Pick a new font size so it will not be larger than the height of label.
        int fontSizeToUse = Math.min(newFontSize, componentHeight);

        // Set the label's font size to the newly determined size.
        label.setFont(new Font(labelFont.getName(), Font.BOLD, fontSizeToUse));
    }
}

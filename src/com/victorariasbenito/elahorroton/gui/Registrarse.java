package com.victorariasbenito.elahorroton.gui;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 *
 */
public class Registrarse extends JDialog {
    private JPanel panel1;
    private Frame owner;

    JTextField txtUsuarioRegistrarse;
    JPasswordField passwordField1Registrarse;
    JPasswordField passwordField2Registrarse;
    JTextField txtNombreRegistrarse;
    JTextField txtApellidosRegistrarse;
    JTextField txtDniRegistrarse;
    JTextField txtEmailRegistrarse;
    JTextField txtTelefonoRegistrarse;
    JButton registrarseButtonRegistrarse;
    JLabel lb1;
    JLabel lb2;
    JLabel lb3;
    JLabel lb4;
    JLabel lb5;
    JLabel lb6;
    JLabel lb7;
    JLabel lb8;

    /**
     * Constructor de la clase.
     * @param owner propietario del dialog
     */
    public Registrarse(Frame owner) {
        super(owner, "Registrarse", true);
        this.owner = owner;

        //Image icon = new ImageIcon(getClass().getResource("src/com/victorariasbenito/elahorroton/base/resources/imagenes/ejemploImagen.png")).getImage();
        Image icon = null;
        try {
            icon = ImageIO.read(new File("src/com/victorariasbenito/elahorroton/resources/imagenes/icono.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.owner.setIconImage(icon);

        initDialog();
        aumentarFuenteComponentes();
    }

    /**
     * Inicializa el JDialog
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+80, this.getHeight()+80));
        this.setLocationRelativeTo(owner);
    }

    /**
     * Aumenta el tamaño de los componentes
     */
    private void aumentarFuenteComponentes() {
        Vista.aumentarFuenteComponent(txtUsuarioRegistrarse);
        Vista.aumentarFuenteComponent(passwordField1Registrarse);
        Vista.aumentarFuenteComponent(passwordField2Registrarse);
        Vista.aumentarFuenteComponent(txtNombreRegistrarse);
        Vista.aumentarFuenteComponent(txtApellidosRegistrarse);
        Vista.aumentarFuenteComponent(txtDniRegistrarse);
        Vista.aumentarFuenteComponent(txtEmailRegistrarse);
        Vista.aumentarFuenteComponent(txtTelefonoRegistrarse);
        Vista.aumentarFuenteComponent(registrarseButtonRegistrarse);
        Vista.aumentarFuenteComponent(lb1);
        Vista.aumentarFuenteComponent(lb2);
        Vista.aumentarFuenteComponent(lb3);
        Vista.aumentarFuenteComponent(lb4);
        Vista.aumentarFuenteComponent(lb5);
        Vista.aumentarFuenteComponent(lb6);
        Vista.aumentarFuenteComponent(lb7);
        Vista.aumentarFuenteComponent(lb8);
    }
}
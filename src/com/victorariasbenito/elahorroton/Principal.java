package com.victorariasbenito.elahorroton;

import com.victorariasbenito.elahorroton.gui.Controlador;
import com.victorariasbenito.elahorroton.gui.Modelo;
import com.victorariasbenito.elahorroton.gui.Vista;

public class Principal {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }
}
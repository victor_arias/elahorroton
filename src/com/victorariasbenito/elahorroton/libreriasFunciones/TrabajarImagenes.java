package com.victorariasbenito.elahorroton.libreriasFunciones;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class TrabajarImagenes {
    /* Mis metodos */

    /*
     * Referencias:
     * http://algoimagen.blogspot.com/2013/09/java-convertir-objetos.html
     * https://mkyong.com/java/how-to-convert-byte-to-bufferedimage-in-java/
     * http://www.java2s.com/Tutorial/Java/0261__2D-Graphics/CreatingaBufferedImagefromanImageobject.htm
     */

    /**
     * convert BufferedImage to byte[]
     *
     * @param icono
     * @return
     * @throws IOException
     */
    public static byte[] IconToBytes(Icon icono) throws IOException {
        BufferedImage bi = iconToBufferedImage(icono);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bi, "png", baos);
        byte[] bytes = baos.toByteArray();
        return bytes;
    }

    /**
     * convert byte[] back to a BufferedImage
     *
     * @param bytes
     * @return
     * @throws IOException
     */
    public static Icon bytesToIcon(byte[] bytes) throws IOException {
        InputStream is = new ByteArrayInputStream(bytes);
        BufferedImage newBi = ImageIO.read(is);
        Icon icon = bufferedImageToIcon(newBi);
        return icon;
    }

    /**
     * Convierte de Icono a BufferdImage
     *
     * @param bufferImage
     * @return
     */
    public static Icon bufferedImageToIcon(BufferedImage bufferImage) {
        ImageIcon imgIcon = new ImageIcon(bufferImage);
        Icon iconReturn = (Icon) (imgIcon);
        return iconReturn;
    }

    /**
     * Convierte de BufferedImage a Icono
     *
     * @param icon
     * @return
     */
    public static BufferedImage iconToBufferedImage(Icon icon) {
        Image img = iconToImage(icon);
        BufferedImage bufferedImage = new BufferedImage(img.getWidth(null), img.getHeight(null),
                BufferedImage.TYPE_INT_RGB);
        return bufferedImage;
    }

    /**
     * Converts a Icon in a Image
     *
     * @param icon Icon is going to be transformed
     * @return returns an Image from a Icon
     */
    public static Image iconToImage(Icon icon) {
        if (icon instanceof ImageIcon) {
            return ((ImageIcon) icon).getImage();
        } else {
            int w = icon.getIconWidth();
            int h = icon.getIconHeight();
            GraphicsEnvironment ge =
                    GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice gd = ge.getDefaultScreenDevice();
            GraphicsConfiguration gc = gd.getDefaultConfiguration();
            BufferedImage image = gc.createCompatibleImage(w, h);
            Graphics2D g = image.createGraphics();
            icon.paintIcon(null, g, 0, 0);
            g.dispose();
            return image;
        }
    }

    /**
     * Convierte una Image en una BufferedImage
     *
     * @param img Imagen a convertir
     * @return BufferedImagen resultante
     */
    public static BufferedImage imageToBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }
        // Creo la BufferedImage con trasparencia
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Dibujamos imagen en la BufferedImage
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        return bimage;
    }

    /**
     * Convierte una Image en un Icon
     *
     * @param image
     * @return
     */
    public static Icon imageToIcon(Image image) {
        ImageIcon imgIcon = new ImageIcon(image);
        Icon iconReturn = (Icon) imgIcon;
        return iconReturn;
    }

    /**
     * Convierte un Icon en un ImageIcon
     *
     * @param icon
     * @return
     */
    public static ImageIcon iconToImageIcon(Icon icon) {
        ImageIcon imageIconRetur = new ImageIcon(iconToImage(icon));
        return imageIconRetur;
    }

    /**
     * Convierte una ImageIcon en un Icon
     *
     * @param imageIcon
     * @return
     */
    public static Icon imageIconToIcon(ImageIcon imageIcon) {
        Icon iconReturn = (Icon) imageIcon;
        return iconReturn;
    }

    /**
     * Convierte BufferedImage en byte[]
     *
     * @param bytes
     * @return
     * @throws IOException
     */
    public static BufferedImage byteToBufferedImage(byte[] bytes) throws IOException {
        InputStream is = new ByteArrayInputStream(bytes);
        BufferedImage newBi = ImageIO.read(is);
        return newBi;
    }

    /**
     * Convierte una imagen en byte[] a BufferedImage
     *
     * @param bufferedImage
     * @return
     * @throws IOException
     */
    public static byte[] bufferedImageToByte(BufferedImage bufferedImage) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", baos);
        byte[] bytes = baos.toByteArray();
        return bytes;
    }

    /**
     * Convierto una imagen BufferedImage a byte[]
     *
     * @param bufferImage
     * @return
     */
    public static Image bufferedImageToImage(BufferedImage bufferImage) {
        Image imgReturn = (Image) bufferImage;
        return imgReturn;
    }

    /**
     * Convierte un dato byte[] a Image
     * @param bytes
     * @return
     * @throws IOException
     */
    public static Image bytesToImage(byte[] bytes) throws IOException {
        return bufferedImageToImage(byteToBufferedImage(bytes));
    }

    /**
     * Convierte un Icon en un byte[]
     * @param icono
     * @return
     * @throws IOException
     */
    public static byte[] iconToBytes(Icon icono) throws IOException {
        return TrabajarImagenes.bufferedImageToByte(TrabajarImagenes.imageToBufferedImage(TrabajarImagenes.iconToImage(icono)));
    }
}

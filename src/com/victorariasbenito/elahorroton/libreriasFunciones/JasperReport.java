package com.victorariasbenito.elahorroton.libreriasFunciones;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import java.sql.Connection;
import java.util.HashMap;

public class JasperReport {

    /**
     * Función que muestra un pdf generado con el fichero .jasper y en caso de recibir una dirección, guarda un pdf
     * @param conexionBaseDatos Dato tipo Connection con una conexión a base de datos
     * @param direccionFicheroOriginal String con la dirección del fichero .jasper
     * @param direccionNuevoFicheroPDF String con la dirección para guardar un pdf
     */
    public static void generarInformePrincipal(Connection conexionBaseDatos, String direccionFicheroOriginal, String direccionNuevoFicheroPDF){
        // TODO Auto-generated method stub
        JasperPrint informeVictor = generarInforme(direccionFicheroOriginal, conexionBaseDatos);
        JasperViewer viewer = new JasperViewer(informeVictor, false);
        viewer.setVisible(true);

        //Si la dirección del nuevo fichero no esta vacia creo un pdf allí
        if((direccionNuevoFicheroPDF != null) && !direccionNuevoFicheroPDF.equals("")){
            try {
                JasperExportManager.exportReportToPdfFile(informeVictor, direccionNuevoFicheroPDF);
            } catch (JRException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /**
     * Función que muestra un pdf generado con el fichero .jasper
     * @param conexionBaseDatos Dato tipo Connection con una conexión a base de datos
     * @param direccionFicheroOriginal String con la dirección del fichero .jasper
     */
    public static void generarInformePrincipal(Connection conexionBaseDatos, String direccionFicheroOriginal){
        generarInformePrincipal(conexionBaseDatos, direccionFicheroOriginal, null);
    }

    /**
     * Función que devuelve un JasperPrint con un fichero de datos
     * @param direccionFichero Dirección de un fichero .jasper
     * @param conexionBaseDatos Dato tipo Connection con una conexión a base de datos
     * @return Devuelve un JasperPrint con los datos
     */
    public static JasperPrint generarInforme(String direccionFichero, Connection conexionBaseDatos) {
        try {
            JasperPrint informe = JasperFillManager.fillReport(direccionFichero, new HashMap(), conexionBaseDatos);
            return informe;
        } catch (JRException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }


}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.victorariasbenito.elahorroton.libreriasFunciones;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.MessageDigest;
import java.util.Base64;

/**
 *
 * @author vbeni
 */
public class Cifrado {
    /**
     * Devuelve un String cifrado del String sin cifrar
     * @param sinCifrar String sin cifrar
     * @return String cifrado
     * @throws Exception
     */
    public static String cifrar(String sinCifrar) throws Exception {
        return byteToString(cifrarB(sinCifrar));
    }

    /**
     * Devuelve un String descifrado del String cifrado
     * @param cifrado String cifrado
     * @return String sin cifrar
     * @throws Exception
     */
    public static String descifrar(String cifrado) throws Exception {
        return descifrarB(stringToByte(cifrado));
    }

    /**
     * Devuelve un String con un hash
     * @param sinCifrar String sin hashear
     * @return String con el hasheo
     * @throws Exception
     */
    public static String hashear(String sinCifrar) throws Exception {
        return byteToString(hashearB(sinCifrar));
    }

    /**
     * Comprueba que el String cifrada corresponde al hash sinCifrar
     * @param cifrada String con el hash
     * @param sinCifrar String del texto a comprobar
     * @return Boolean Devuelve True si se corresponde el texto con el hash y false en caso contrario
     * @throws Exception
     */
    public static boolean comprobarHasheo(String cifrada, String sinCifrar) throws Exception {
        return comprobarHasheoB(stringToByte(cifrada), sinCifrar);
    }

    private static byte[] cifrarB(String sinCifrar) throws Exception {
        final byte[] bytes = sinCifrar.getBytes("UTF-8");
        final Cipher aes = obtieneCipher(true);
        final byte[] cifrado = aes.doFinal(bytes);
        return cifrado;
    }

    private static String descifrarB(byte[] cifrado) throws Exception {
        final Cipher aes = obtieneCipher(false);
        final byte[] bytes = aes.doFinal(cifrado);
        final String sinCifrar = new String(bytes, "UTF-8");
        return sinCifrar;
    }

    private static Cipher obtieneCipher(boolean cifrar) throws Exception {
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(128);
        Key key = keyGenerator.generateKey();

        key = new SecretKeySpec("una clave de 16 bytes".getBytes(),  0, 16, "AES");
        final Cipher aes = Cipher.getInstance("AES/ECB/PKCS5Padding");

        if (cifrar) {
                aes.init(Cipher.ENCRYPT_MODE, key);
        } else {
                aes.init(Cipher.DECRYPT_MODE, key);
        }

        return aes;
    } 
    
    private static byte[] hashearB(String sinCifrar) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.update(sinCifrar.getBytes());
        //Se realiza el Hashing
        byte[] mb = md.digest();;
        return mb;
    }

    private static boolean comprobarHasheoB(byte[] cifrada, String sinCifrar) throws Exception {
        byte[] sCifrar = hashearB(sinCifrar);
        if(cifrada.length == sCifrar.length){
            for (int i = 0; i < cifrada.length; i++) {
                if(sCifrar[i] != cifrada[i]){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private static String byteToString(byte[] byteArray){
        //return new String(byteArray, StandardCharsets.UTF_8);
        return Base64.getEncoder().encodeToString(byteArray);
    }
    
    private static byte[] stringToByte(String s){
       return Base64.getDecoder().decode(s);
    }
   
}
package com.victorariasbenito.elahorroton.gui;

import com.victorariasbenito.elahorroton.clases.*;
import com.victorariasbenito.elahorroton.libreriasFunciones.TrabajarImagenes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class ModeloTest {

    Modelo modelo = new Modelo();

    @Test
    public void altaUsuarioGetUsuarioModificarUsuarioEliminarUsuario() {
        this.modelo.conectar();
        Usuario usuario = new Usuario();
        usuario.setId(10000);
        usuario.setNombre("u1");
        usuario.setApellidos("u2");
        usuario.setDni("u3");
        usuario.setEmail("u4");
        usuario.setNick("u5");
        usuario.setPass("u6");
        usuario.setTelefono("u7");
        usuario.setAdministrador(true);

        /**/
        modelo.altaUsuario(usuario);
        Usuario usuario2 = modelo.getUsuario(usuario.getNick());
        Assertions.assertEquals(usuario, usuario2);


        /**/
        usuario2.setAdministrador(false);
        modelo.modificarUsuario(usuario2);
        Usuario usuario3 = modelo.getUsuario(usuario2.getNick());
        Assertions.assertEquals(usuario3.isAdministrador(), usuario2.isAdministrador());

        /**/
        modelo.borrarUsuario(usuario3);
        Usuario usuario4 = modelo.getUsuario(usuario3.getNick());
        Assertions.assertNull(usuario4);
    }

    @Test
    public void altaSeccionGetSeccionesModificarSeccionBorrarSeccion() {
        this.modelo.conectar();
        Seccion seccion = new Seccion();
        seccion.setId(10000);
        seccion.setNombre("s1");
        seccion.setDescripcion("s2");
        seccion.setEspacioOcupado(2);
        seccion.setEspacioTotal(3);
        seccion.setHabilitada(true);
        seccion.setHoraApertura(Time.valueOf(LocalTime.now()));
        seccion.setHoraCierre(Time.valueOf(LocalTime.now()));

        /**/
        modelo.altaSeccion(seccion);
        ArrayList<Seccion> secciones = modelo.getSecciones();
        Seccion seccion1 =null;
        for(Seccion s : secciones){
            if(s.getId() == seccion.getId()){
                seccion1 = s;
            }
        }
        Assertions.assertEquals(seccion1, seccion);


        /**/
        seccion.setNombre("sN2");
        modelo.modificarSeccion(seccion);
        secciones = modelo.getSecciones();
        seccion1 =null;
        for(Seccion s : secciones){
            if(s.getId() == seccion.getId()){
                seccion1 = s;
            }
        }
        Assertions.assertEquals(seccion1.getNombre(), seccion.getNombre());

        /**/
        modelo.borrarSeccion(seccion);
        secciones = modelo.getSecciones();
        seccion1 =null;
        for(Seccion s : secciones){
            if(s.getId() == seccion.getId()){
                seccion1 = s;
            }
        }
        Assertions.assertNull(seccion1);
    }


    @Test
    public void altaProductoGetProductosModificarProductoEliminarProducto() throws IOException {
        this.modelo.conectar();
        Seccion seccion = new Seccion();
        seccion.setId(11230);
        seccion.setNombre("s1");
        seccion.setDescripcion("s2");
        seccion.setEspacioOcupado(2);
        seccion.setEspacioTotal(3);
        seccion.setHabilitada(true);
        seccion.setHoraApertura(Time.valueOf(LocalTime.now()));
        seccion.setHoraCierre(Time.valueOf(LocalTime.now()));
        modelo.altaSeccion(seccion);

        Producto producto = new Producto();
        producto.setId(12312);
        producto.setDescripcion("p11223");
        producto.setFechaCaducidad(Date.valueOf(LocalDate.now()));
        producto.setMarca("p2213");
        producto.setNombre("p123");
        producto.setPrecio(22.0);
        producto.setSeccion(seccion);

        Image imagen = ImageIO.read(new File("src/com/victorariasbenito/elahorroton/resources/imagenes/ejemploImagen.png"));
        ImageIcon imageIcon = new ImageIcon(imagen.getScaledInstance(150, 150, Image.SCALE_SMOOTH));
        Icon icon = TrabajarImagenes.imageIconToIcon(imageIcon);

        byte[] bt = TrabajarImagenes.iconToBytes(icon);
        producto.setImagen(bt);


        /**/
       modelo.altaProducto(producto);
        ArrayList<Producto> productos = modelo.getProductos();
        Producto producto1 =null;
        for(Producto p : productos){
            if(p.getId() == producto.getId()){
                producto1 = p;
            }
        }
        Assertions.assertEquals(producto1, producto);


        /**/
        producto.setNombre("pN2");
        modelo.modificarProducto(producto);
        productos = modelo.getProductos();
        producto1 =null;
        for(Producto p : productos){
            if(p.getId() == producto.getId()){
                producto1 = p;
            }
        }
        Assertions.assertEquals(producto1.getNombre(), producto.getNombre());

        /**/
        modelo.borrarProducto(producto);
        productos = modelo.getProductos();
        producto1 =null;
        for(Producto p : productos){
            if(p.getId() == producto.getId()){
                producto1 = p;
            }
        }
        Assertions.assertNull(producto1);
        modelo.borrarSeccion(seccion);

    }


    @Test
    public void altaFacturaGetFacturasModificarFacturaBorrarFactura() {
        this.modelo.conectar();

        Usuario usuario = new Usuario();
        usuario.setId(14114);
        usuario.setNombre("u1");
        usuario.setApellidos("u2");
        usuario.setDni("u3");
        usuario.setEmail("u4");
        usuario.setNick("u5");
        usuario.setPass("u6");
        usuario.setTelefono("u7");
        usuario.setAdministrador(true);
        modelo.altaUsuario(usuario);

        Factura factura = new Factura();
        factura.setId(1310);
        factura.setCodigo("f1");
        factura.setFechaCreacion(LocalDateTime.of(123,11,12,7,14));
        factura.setCambio(1.0);
        factura.setDescuento(2.0);
        factura.setPago(3.0);
        factura.setPrecioTotal(4.0);
        factura.setUsuario(usuario);

        /**/
        modelo.altaFactura(factura);
        ArrayList<Factura> facturas = modelo.getFacturas();
        Factura factura1 =null;
        for(Factura f : facturas){
            if(f.getId() == factura.getId()){
                factura1 = f;
            }
        }
        System.out.println(factura.getId());
        System.out.println(factura.getCodigo());
        System.out.println(factura.getFechaCreacion());
        System.out.println(factura.getCambio());
        System.out.println(factura.getDescuento());
        System.out.println(factura.getPago());
        System.out.println(factura.getPrecioTotal() + "_____");

        System.out.println(factura1.getId());
        System.out.println(factura1.getCodigo());
        System.out.println(factura1.getFechaCreacion());
        System.out.println(factura1.getCambio());
        System.out.println(factura1.getDescuento());
        System.out.println(factura1.getPago());
        System.out.println(factura1.getPrecioTotal());
        Assertions.assertEquals(factura1, factura);


        /**/
        factura.setCodigo("fN2");
        modelo.modificarFactura(factura);
        facturas = modelo.getFacturas();
        factura1 = null;
        for(Factura f : facturas){
            if(f.getId() == factura.getId()){
                factura1 = f;
            }
        }
        Assertions.assertEquals(factura1.getCodigo(), factura.getCodigo());

        /**/
        modelo.borrarFactura(factura);
        facturas = modelo.getFacturas();
        factura1 =null;
        for(Factura f : facturas){
            if(f.getId() == factura.getId()){
                factura1 = f;
            }
        }
        Assertions.assertNull(factura1);
        modelo.borrarUsuario(usuario);
    }

}